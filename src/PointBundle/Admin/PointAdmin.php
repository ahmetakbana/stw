<?php

namespace PointBundle\Admin;

use PointBundle\Entity\Point;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ProxyQueryInterface;
use Sonata\AdminBundle\Form\FormMapper;
use TravelBundle\Entity\StepRepository;

class PointAdmin extends Admin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->tab('General')
                ->with('General')
                    ->add('name')
                    ->add('address')
                ->end()
            ->end()
            ->tab('Image')
                ->with('Image')
                    ->add('image', 'sonata_media_type', array(
                        'provider' => 'sonata.media.provider.image',
                        'context' => 'place',
                    ))
                ->end()
            ->end()
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('name')
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('name')
            ->add('creator')
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function preRemove($point)
    {
        $this->removeTimeLine($point);
        parent::preRemove($point);
    }

    /**
     * {@inheritdoc}
     */
    public function preBatchAction($actionName, ProxyQueryInterface $query, array &$idx, $allElements)
    {
        if ('delete' == $actionName) {

            $container = $this->getConfigurationPool()->getContainer();
            $pointRepository = $container->get('point.repository.point');

            foreach ($idx as $pointId) {
                $point = $pointRepository->findOneBy(array('id' => $pointId));
                $this->removeTimeLine($point);
            }
        }
    }

    /**
     * @param Point $point
     */
    protected function removeTimeLine(Point $point)
    {
        $container = $this->getConfigurationPool()->getContainer();

        /** @var StepRepository $stepRepository */
        $stepRepository = $container->get('travel.repository.step');

        $steps = $stepRepository->getByPoint($point);

        $timeLineHandler = $container->get('time_line.handler');

        foreach ($steps as $step) {
            $timeLineHandler->userRemoveStep($step);
        }
    }
}
<?php

namespace SiteMapBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;

class SiteMapController extends Controller
{
    /**
     * @Route("/sitemap.xml")
     */
    public function siteMapAction()
    {
        $travelRepository = $this->get('travel.repository.travel');
        $travels = $travelRepository->findAll();

        $placeRepository = $this->get('place.repository.place');
        $places = $placeRepository->findAll();

        $userRepository = $this->get('user.repository.user');
        $users = $userRepository->getPublic();

        $response = new Response();
        $response->headers->set('Content-Type', 'xml');
        return $this->render('SiteMapBundle:SiteMap:site-map.xml.twig', array(
            'places' => $places,
            'travels' => $travels,
            'users' => $users
        ), $response);
    }
}

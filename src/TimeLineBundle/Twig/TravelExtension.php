<?php

namespace TimeLineBundle\Twig;

use PlaceBundle\Entity\Place;
use PlaceBundle\Entity\PlaceRepository;
use TravelBundle\Entity\Step;
use TravelBundle\Entity\StepRepository;

class TravelExtension extends \Twig_Extension
{
    /**
     * @var StepRepository
     */
    private $stepRepository;

    /**
     * @var PlaceRepository
     */
    private $placeRepository;

    /**
     * @param StepRepository $stepRepository
     * @param PlaceRepository $placeRepository
     */
    public function __construct(StepRepository $stepRepository, PlaceRepository $placeRepository)
    {
        $this->stepRepository = $stepRepository;
        $this->placeRepository = $placeRepository;
    }

    public function getFunctions()
    {
        return array(
            'getStep' => new \Twig_Filter_Method($this, 'getStep'),
            'getPlace' => new \Twig_Filter_Method($this, 'getPlace')
        );
    }

    /**
     * @param $stepId
     * @return Step
     */
    public function getStep($stepId)
    {
        return $this->stepRepository->find($stepId);
    }

    /**
     * @param $placeId
     * @return Place
     */
    public function getPlace($placeId)
    {
        return $this->placeRepository->find($placeId);
    }

    public function getName()
    {
        return 'travel_extension';
    }
}
<?php

namespace TimeLineBundle\Handler;

use Application\Sonata\UserBundle\Entity\User;
use PlaceBundle\Entity\Place;
use PlaceBundle\Entity\PlaceItem;
use TimelineBundle\Driver\ORM\ActionManager;
use Spy\TimelineBundle\Driver\ORM\TimelineManager;
use TravelBundle\Entity\GeneralNote;
use TravelBundle\Entity\Step;
use TravelBundle\Entity\StepNote;
use TravelBundle\Entity\Travel;

class TimeLineHandler
{
    /**
     * @var ActionManager
     */
    private $actionManager;

    /**
     * @var TimelineManager
     */
    private $timeLineManager;

    /**
     * @param ActionManager $actionManager
     * @param TimelineManager $timeLineManager
     */
    public function __construct(ActionManager $actionManager, TimelineManager $timeLineManager)
    {
        $this->actionManager = $actionManager;
        $this->timeLineManager = $timeLineManager;
    }

    /**
     * @param User $user
     * @param Travel $travel
     * @throws \Exception
     */
    public function userCreateTravel(User $user, Travel $travel)
    {
        // Travel based component
        $subject = $this->actionManager->findComponentOrCreateByDirect($user, array(
            'travelId' => $travel->getId()
        ));
        $action = $this->actionManager->create($subject, 'create', array('directComplement' => $travel));
        $this->actionManager->updateAction($action);

        /*
        // User based component
        $subject = $this->actionManager->findOrCreateComponent($user, array(
            'userId' => $user->getId()
        ));
        $action = $this->actionManager->create($subject, 'create', array('directComplement' => $travel));
        $this->actionManager->updateAction($action);
        */
    }

    /**
     * @param Travel $travel
     */
    public function userRemoveTravel(Travel $travel)
    {
        foreach ($travel->getSteps() as $step) {
            $this->userRemoveStep($step);
        }

        foreach ($travel->getGeneralNotes() as $generalNote) {
            $this->userRemoveGeneralNote($generalNote);
        }

        $user = $travel->getUser();
        $subject = $this->actionManager->findComponentOrCreateByDirect($user, array(
            'travelId' => $travel->getId()
        ));
        $this->removeTimeLine($subject, $travel);

        /*
        $subject = $this->actionManager->findComponentOrCreateByDirect($user, array(
            'userId' => $user->getId()
        ));
        $this->removeTimeLine($subject, $travel);
        */
    }

    /**
     * @param User $user
     * @param Travel $travel
     * @param Step $step
     * @throws \Exception
     */
    public function userAddStep(User $user, Travel $travel, Step $step)
    {
        // Travel based component
        $subject = $this->actionManager->findComponentOrCreateByDirect($user, array(
            'travelId' => $travel->getId()
        ));
        $action = $this->actionManager->create($subject, 'add', array(
            'directComplement' => $step,
            'inDirectComplement' => $travel
        ));
        $this->actionManager->updateAction($action);

        /*
        // User based component
        $subject = $this->actionManager->findOrCreateComponent($user, array(
            'userId' => $user->getId()
        ));
        $action = $this->actionManager->create($subject, 'add', array(
            'directComplement' => $step,
            'inDirectComplement' => $travel
        ));
        $this->actionManager->updateAction($action);
        */
    }

    /**
     * @param Step $step
     */
    public function userRemoveStep(Step $step)
    {
        foreach ($step->getPlaceItems() as $placeItem) {
            $this->userRemovePlace($placeItem);
        }

        foreach ($step->getStepNotes() as $stepNote) {
            $this->userRemoveStepNote($stepNote);
        }

        $travel = $step->getTravel();
        $user = $step->getCreator();
        $subject = $this->actionManager->findComponentOrCreateByDirect($user, array(
            'travelId' => $travel->getId()
        ));

        $this->removeTimeLine($subject, $step);

        /*
        $subject = $this->actionManager->findOrCreateComponent($user, array(
            'userId' => $user->getId()
        ));
        $this->removeTimeLine($subject, $step);
        */
    }

    /**
     * @param User $user
     * @param Travel $travel
     * @param GeneralNote $generalNote
     * @throws \Exception
     */
    public function userAddGeneralNote(User $user, Travel $travel, GeneralNote $generalNote)
    {
        // Travel based component
        $subject = $this->actionManager->findComponentOrCreateByDirect($user, array(
            'travelId' => $travel->getId()
        ));
        $action = $this->actionManager->create($subject, 'add', array(
            'directComplement' => $generalNote,
            'inDirectComplement' => $travel
        ));
        $this->actionManager->updateAction($action);

        /*
        // User based component
        $subject = $this->actionManager->findOrCreateComponent($user, array(
            'userId' => $user->getId()
        ));
        $action = $this->actionManager->create($subject, 'add', array(
            'directComplement' => $generalNote,
            'inDirectComplement' => $travel
        ));
        $this->actionManager->updateAction($action);
        */
    }

    /**
     * @param GeneralNote $generalNote
     */
    public function userRemoveGeneralNote(GeneralNote $generalNote)
    {
        $travel = $generalNote->getTravel();

        $user = $generalNote->getUser();

        $subject = $this->actionManager->findComponentOrCreateByDirect($user, array(
            'travelId' => $travel->getId()
        ));
        $this->removeTimeLine($subject, $generalNote);

        /*
        $subject = $this->actionManager->findOrCreateComponent($user, array(
            'userId' => $user->getId()
        ));
        $this->removeTimeLine($subject, $generalNote);
        */
    }

    /**
     * @param User $user
     * @param Travel $travel
     * @param StepNote $stepNote
     * @throws \Exception
     */
    public function userAddStepNote(User $user, Travel $travel, StepNote $stepNote)
    {
        // Travel based component
        $subject = $this->actionManager->findComponentOrCreateByDirect($user, array(
            'travelId' => $travel->getId()
        ));
        $action = $this->actionManager->create($subject, 'add', array(
            'directComplement' => $stepNote,
            'inDirectComplement' => $stepNote->getStep()
        ));
        $this->actionManager->updateAction($action);

        /*
        // User based component
        $subject = $this->actionManager->findOrCreateComponent($user, array(
            'userId' => $user->getId()
        ));
        $action = $this->actionManager->create($subject, 'add', array(
            'directComplement' => $stepNote,
            'inDirectComplement' => $stepNote->getStep()
        ));
        $this->actionManager->updateAction($action);
        */
    }

    /**
     * @param StepNote $generalNote
     */
    public function userRemoveStepNote(StepNote $stepNote)
    {
        $travel = $stepNote->getStep()->getTravel();

        $user = $stepNote->getUser();

        $subject = $this->actionManager->findComponentOrCreateByDirect($user, array(
            'travelId' => $travel->getId()
        ));
        $this->removeTimeLine($subject, $stepNote);

        /*
        $subject = $this->actionManager->findOrCreateComponent($user, array(
            'userId' => $user->getId()
        ));
        $this->removeTimeLine($subject, $stepNote);
        */
    }

    /**
     * @param User $user
     * @param Step $step
     * @param Place $place
     * @throws \Exception
     */
    public function userAddPlace(User $user, Step $step, Place $place)
    {
        $travel = $step->getTravel();

        // Travel based component
        $subject = $this->actionManager->findComponentOrCreateByDirect($user, array(
            'travelId' => $travel->getId()
        ));
        $action = $this->actionManager->create($subject, 'add', array(
            'directComplement' => $place,
            'inDirectComplement' => $step
        ));
        $this->actionManager->updateAction($action);

        /*
        // User based component
        $subject = $this->actionManager->findOrCreateComponent($user, array(
            'userId' => $user->getId()
        ));
        $action = $this->actionManager->create($subject, 'add', array(
            'directComplement' => $place,
            'inDirectComplement' => $step
        ));
        $this->actionManager->updateAction($action);
        */
    }

    /**
     * @param Place $place
     */
    public function userRemovePlace(PlaceItem $placeItem)
    {
        $travel = $placeItem->getStep()->getTravel();

        $place = $placeItem->getPlace();

        $user = $placeItem->getCreator();

        $subject = $this->actionManager->findComponentOrCreateByDirect($user, array(
            'travelId' => $travel->getId()
        ));
        $this->removeTimeLine($subject, $place);

        /*
        $subject = $this->actionManager->findOrCreateComponent($user, array(
            'userId' => $user->getId()
        ));
        $this->removeTimeLine($subject, $place);
        */
    }

    /**
     * @param Travel $travel
     * @return array|\Traversable
     */
    public function getTravelTimeLine(Travel $travel, $limit = 10)
    {
        $subject = $this->actionManager->findComponentOrCreateByDirect($travel->getUser(), array(
            'travelId' => $travel->getId()
        ));

        return $this->timeLineManager->getTimeline($subject, array('max_per_page' => $limit));
    }

    /**
     * @param $subject
     * @param $componentObject
     * @throws \Exception
     */
    protected function removeTimeLine($subject, $componentObject)
    {
        $timeLine = $this->timeLineManager->getTimeline($subject);

        foreach ($timeLine as $action) {
            foreach ($action->getActionComponents()->getIterator() as $actionComponent) {
                $component = $actionComponent->getComponent();
                if (
                    $componentObject->getId() == $component->getIdentifier()[0] &&
                    str_replace('Proxies\__CG__\\', '', get_class($componentObject)) == $component->getModel()
                ) {
                    $this->timeLineManager->remove($subject, $action->getId());
                }
            }
        }

        $this->timeLineManager->flush();
    }
}
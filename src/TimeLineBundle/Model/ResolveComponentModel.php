<?php

namespace TimeLineBundle\Model;

use Spy\Timeline\Exception\ResolveComponentDataException;
use Spy\Timeline\ResolveComponent\ValueObject\ResolveComponentModelIdentifier;

class ResolveComponentModel extends ResolveComponentModelIdentifier
{
    private $model;

    private $identifier;

    /**
     * @param string|object     $model
     * @param null|string|array $identifier
     */
    public function __construct($model, $identifier = null)
    {
        $this->guardValidModelAndIdentifier($model, $identifier);
        $this->model = $model;
        $this->identifier = $identifier;
    }

    /**
     * Gets the model.
     *
     * @return object|string
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * Gets the identifier.
     *
     * @return array|null|string
     */
    public function getIdentifier()
    {
        return $this->identifier;
    }

    /**
     * @param $model
     * @param $identifier
     *
     * @throws \Spy\Timeline\Exception\ResolveComponentDataException
     */
    private function guardValidModelAndIdentifier($model, $identifier)
    {
        if (empty($model) || (!is_object($model) && (null === $identifier || '' === $identifier))) {
            throw new ResolveComponentDataException('Model has to be an object or (a scalar + an identifier in 2nd argument)');
        }
    }
}
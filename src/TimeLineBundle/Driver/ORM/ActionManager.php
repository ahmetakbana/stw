<?php

namespace TimeLineBundle\Driver\ORM;

use Spy\Timeline\ResolveComponent\ValueObject\ResolvedComponentData;
use Spy\TimelineBundle\Driver\ORM\ActionManager as BaseManager;
use TimeLineBundle\Model\ResolveComponentModel;

class ActionManager extends BaseManager
{
    public function findComponentOrCreateByDirect($model, $identifier, $flush = true)
    {
        $resolvedComponentData = $this->resolveModelAndIdentifier($model, $identifier);

        $component = $this->getComponentRepository()
            ->createQueryBuilder('c')
            ->where('c.model = :model')
            ->andWhere('c.identifier = :identifier')
            ->setParameter('model', $resolvedComponentData->getModel())
            ->setParameter('identifier', serialize($resolvedComponentData->getIdentifier()))
            ->getQuery()
            ->getOneOrNullResult()
        ;

        if ($component) {
            $component->setData($resolvedComponentData->getData());

            return $component;
        }

        return $this->createComponentFromResolvedComponentDataDirect($resolvedComponentData, $flush, $identifier);
    }

    protected function createComponentFromResolvedComponentDataDirect
    (
        ResolvedComponentData $resolved,
        $flush = true,
        $direct = null
    )
    {
        $component = $this->getComponentFromResolvedComponentData($resolved);

        if (null != $direct) {
            $component->setDirectIdentifier(serialize($direct));
        }

        $this->objectManager->persist($component);

        if ($flush) {
            $this->flushComponents();
        }

        return $component;
    }

    /**
     * Resolves the model and identifier.
     *
     * @param string|object     $model
     * @param null|string|array $identifier
     *
     * @return ResolvedComponentData
     */
    protected function resolveModelAndIdentifier($model, $identifier)
    {
        $resolve = new ResolveComponentModel($model, $identifier);

        return $this->getComponentDataResolver()->resolveComponentData($resolve);
    }
}
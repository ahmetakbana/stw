<?php

namespace TimeLineBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class TimeLineBundle extends Bundle
{
    public function getParent()
    {
        return 'SpyTimelineBundle';
    }
}

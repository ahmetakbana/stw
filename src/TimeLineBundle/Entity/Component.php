<?php

namespace TimeLineBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Spy\TimelineBundle\Entity\Component as BaseComponent;

/**
 * Component
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Component extends BaseComponent
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(name="direct_identifier", type="string", nullable=true)
     */
    protected $directIdentifier;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getDirectIdentifier()
    {
        return $this->directIdentifier;
    }

    /**
     * @param string $directIdentifier
     * @return Component
     */
    public function setDirectIdentifier($directIdentifier)
    {
        $this->directIdentifier = $directIdentifier;

        return $this;
    }
}

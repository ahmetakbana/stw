<?php

namespace TimeLineBundle\ResolveComponent;

use Spy\Timeline\ResolveComponent\ValueObject\ResolvedComponentData;
use Spy\Timeline\Exception\ResolveComponentDataException;
use Spy\Timeline\ResolveComponent\ValueObject\ResolveComponentModelIdentifier;

use Spy\TimelineBundle\ResolveComponent\DoctrineComponentDataResolver as BaseResolver;

class DoctrineComponentDataResolver extends BaseResolver
{
    /**
     * {@inheritdoc}
     */
    public function resolveComponentData(ResolveComponentModelIdentifier $resolve)
    {
        $model = $resolve->getModel();
        $identifier = $resolve->getIdentifier();
        $data = null;

        if (is_object($model)) {
            $data = $model;
            $modelClass = get_class($model);
            $metadata = $this->getClassMetadata($modelClass);

            // if object is linked to doctrine
            if (null !== $metadata) {
                $fields = $metadata->getIdentifier();
                if (!is_array($fields)) {
                    $fields = array($fields);
                }
                $many = count($fields) > 1;

                $identifier = array();
                foreach ($fields as $field) {
                    $getMethod = sprintf('get%s', ucfirst($field));
                    $value = (string) $model->{$getMethod}();

                    //Do not use it: https://github.com/stephpy/TimelineBundle/issues/59
                    //$value = (string) $metadata->reflFields[$field]->getValue($model);

                    if (empty($value)) {
                        throw new ResolveComponentDataException(sprintf('Field "%s" of model "%s" return an empty result, model has to be persisted.', $field, $modelClass));
                    }

                    $identifier[$field] = $value;
                }

                if (!$many) {
                    $identifier = current($identifier);
                }

                $model = $metadata->getName();
            } else {
                if (!method_exists($model, 'getId')) {
                    throw new ResolveComponentDataException('Model must have a getId method.');
                }

                $identifier = $model->getId();
                $model = $modelClass;
            }
        }

        $identifier = array($identifier, $resolve->getIdentifier());

        return new ResolvedComponentData($model, $identifier, $data);
    }
}

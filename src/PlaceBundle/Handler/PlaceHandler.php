<?php

namespace PlaceBundle\Handler;

use AppBundle\Handler\MediaHandler;
use Application\Sonata\UserBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use PlaceBundle\Entity\Place;
use PlaceBundle\Entity\PlaceItem;
use PlaceBundle\Entity\Review;
use TimeLineBundle\Handler\TimeLineHandler;
use TravelBundle\Entity\Step;
use PlaceBundle\Entity\Type;

class PlaceHandler
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var TimeLineHandler
     */
    private $timeLineHandler;

    /**
     * @var MediaHandler
     */
    private $mediaHandler;

    /**
     * @param EntityManager $entityManager
     * @param TimeLineHandler $timeLineHandler
     */
    public function __construct(EntityManager $entityManager, TimeLineHandler $timeLineHandler, MediaHandler $mediaHandler)
    {
        $this->entityManager = $entityManager;
        $this->timeLineHandler = $timeLineHandler;
        $this->mediaHandler = $mediaHandler;
    }

    /**
     * @param User $user
     * @param Place $place
     * @param Step $step
     * @param array $location
     */
    public function create(User $user, Place $place, Step $step, array $location)
    {
        $existingPlace = $this->getByPlaceMapId($location[5]);

        $types = $this->findTypes();

        if (null == $existingPlace) {
            $place->setPoint($step->getPoint());
            $place->setName($location[0]);
            $place->setLatitude($location[1]);
            $place->setLongitude($location[2]);
            $place->setAddress($location[3]);
            if ('' != $location[4]) {
                $media = $this->mediaHandler->createFromUrl($location[4], $this->entityManager, 'place');
            } else {
                $media = $this->mediaHandler->createDefaultPlaceImage($this->entityManager);
            }
            $place->setImage($media);
            $place->setPlaceMapId($location[5]);
            $place->setCreator($user);

            foreach ($types as $type) {
                if (in_array($type->getType(), $location[6])) {
                    $place->addType($type);
                }
            }

            if (isset($location[7])) {
                foreach ($location[7] as $placeReview) {
                    if ('' != $placeReview->text) {
                        $review = new Review();
                        $review->setPlace($place);
                        $review->setRating($placeReview->aspects[0]->rating);
                        $review->setAuthor($placeReview->author_name);
                        $review->setAuthorUrl($placeReview->author_url);
                        $review->setLanguage($placeReview->language);
                        $review->setText($placeReview->text);
                        $date = new \DateTime();
                        $date->modify('@' . $placeReview->time);
                        $review->setDate($date);

                        $this->entityManager->persist($review);
                    }
                }
            }

            if (0 == $place->getTypes()->count()) {
                if (!empty($location[6])) {
                    $place->setTypeString(@serialize($location[6]));
                }
            }

            $this->entityManager->persist($place);
        } else {
            $place = $existingPlace;
        }

        $place->setPopularity($place->getPopularity()+1);

        $placeItem = new PlaceItem();
        $placeItem->setPlace($place);
        $placeItem->setStep($step);
        $placeItem->setCreator($user);

        $this->entityManager->persist($placeItem);
        $this->entityManager->persist($step);
        $this->entityManager->flush();

        $this->timeLineHandler->userAddPlace($user, $step, $place);
    }

    /**
     * @param integer $placeMapId
     * @return null|Place
     */
    public function getByPlaceMapId($placeMapId)
    {
        $placeRepository = $this->entityManager->getRepository('PlaceBundle:Place');

        return $placeRepository->findOneBy(array('placeMapId' => $placeMapId));
    }

    /**
     * @param int $placeId
     * @return null|Place
     */
    public function getById($placeId)
    {
        $placeRepository = $this->entityManager->getRepository('PlaceBundle:Place');

        return $placeRepository->findOneBy(array('id' => $placeId));
    }

    /**
     * @return Type[]
     */
    public function findTypes()
    {
        $placeTypeRepository = $this->entityManager->getRepository('PlaceBundle:Type');

        return $placeTypeRepository->findAll();
    }
}
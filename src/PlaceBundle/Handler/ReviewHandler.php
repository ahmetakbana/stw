<?php

namespace PlaceBundle\Handler;

use Application\Sonata\UserBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use PlaceBundle\Entity\PlaceItem;
use PlaceBundle\Entity\Review;

class ReviewHandler
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param int $reviewId
     * @return null|Review
     */
    public function getById($reviewId)
    {
        $reviewRepository = $this->entityManager->getRepository('PlaceBundle:Review');

        return $reviewRepository->findOneBy(array('id' => $reviewId));
    }

    /**
     * @param Review $review
     */
    public function save(Review $review)
    {
        $this->entityManager->persist($review);
        $this->entityManager->flush();
    }

    /**
     * @param PlaceItem $placeItem
     */
    public function remove(Review $reviewId)
    {
        $this->entityManager->remove($reviewId);
        $this->entityManager->flush();
    }

    /**
     * @param User $user
     * @param Review $review
     * @return bool
     */
    public function isOwner(User $user, Review $review)
    {
        return $user->getId() == $review->getCreator()->getId();
    }
}
<?php

namespace PlaceBundle\Handler;

use Application\Sonata\UserBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use PlaceBundle\Entity\PlaceItem;
use TimeLineBundle\Handler\TimeLineHandler;
use TravelBundle\Entity\Step;

class PlaceItemHandler
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var TimeLineHandler
     */
    private $timeLineHandler;

    /**
     * @param EntityManager $entityManager
     * @param TimeLineHandler $timeLineHandler
     */
    public function __construct(EntityManager $entityManager, TimeLineHandler $timeLineHandler)
    {
        $this->entityManager = $entityManager;
        $this->timeLineHandler = $timeLineHandler;
    }

    /**
     * @param int $ste$placeItemId
     * @return null|PlaceItem
     */
    public function getById($placeItemId)
    {
        $placeItemRepository = $this->entityManager->getRepository('PlaceBundle:PlaceItem');

        return $placeItemRepository->findOneBy(array('id' => $placeItemId));
    }

    /**
     * @param PlaceItem $placeItem
     */
    public function remove(PlaceItem $placeItem)
    {
        $this->timeLineHandler->userRemovePlace($placeItem);

        $this->entityManager->remove($placeItem);
        $this->entityManager->flush();
    }
}
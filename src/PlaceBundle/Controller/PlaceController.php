<?php

namespace PlaceBundle\Controller;

use PlaceBundle\Entity\Place;
use PlaceBundle\Form\PlaceType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class PlaceController extends Controller
{
    /**
     * @Route("/travel/{stepSlug}/find-place/{stepId}", name="travel_find_place", requirements={"stepId" = "\d+"})
     *
     * @param int $stepId
     */
    public function findAction($stepId)
    {
        $stepHandler = $this->container->get('travel.handler.step');

        $step = $stepHandler->getById($stepId);

        if (null == $step) {
            throw new NotFoundHttpException('Step Not Found!');
        }

        $travel = $step->getTravel();

        $user = $this->getUser();

        if (null == $travel || null == $user) {
            throw new NotFoundHttpException('Travel Not Found');
        }

        $travelHandler = $this->container->get('travel.handler.travel');

        $isOwner = $travelHandler->isOwner($user, $travel);

        if (!$isOwner) {
            throw new AccessDeniedException('Permission Denied!');
        }

        $place = new Place();

        $form = $this->createForm(new PlaceType(), $place);

        $request = $this->container->get('request');

        if($request->getMethod() == 'POST') {
            $form->bind($request);

            if ($form->isValid()) {

                $locationJSON = $request->request->get('location');
                $location = json_decode($locationJSON);

                $placeHandler = $this->container->get('place.handler.place');

                $placeHandler->create($user, $place, $step, $location[0]);

                return $this->redirectToRoute('travel_show_step', array(
                    'slug' => $travel->getSlug(),
                    'stepSlug' => $step->getSlug(),
                    'stepId' => $step->getId()
                ));
            }
        }

        return $this->render('PlaceBundle:Place:find.html.twig', array(
            'form' => $form->createView(),
            'travel' => $travel,
            'step' => $step,
            'user' => $user,
            'isOwner' => $isOwner
        ));
    }

    /**
     * @Route("/travel/{stepSlug}/add-place/{stepId}", name="travel_add_place", requirements={"stepId" = "\d+"})
     *
     * @param int $stepId
     */
    public function addAction($stepId)
    {
        $stepHandler = $this->container->get('travel.handler.step');

        $step = $stepHandler->getById($stepId);

        if (null == $step) {
            throw new NotFoundHttpException('Step Not Found!');
        }

        $travel = $step->getTravel();

        $user = $this->getUser();

        if (null == $travel || null == $user) {
            throw new NotFoundHttpException('Travel Not Found');
        }

        $travelHandler = $this->container->get('travel.handler.travel');

        $isOwner = $travelHandler->isOwner($user, $travel);

        if (!$isOwner) {
            throw new AccessDeniedException('Permission Denied!');
        }

        $placeHandler = $this->container->get('place.handler.place');

        $types = $placeHandler->findTypes();

        $place = new Place();

        $form = $this->createForm(new PlaceType(), $place);

        $request = $this->container->get('request');

        if($request->getMethod() == 'POST') {
            $form->bind($request);

            if ($form->isValid()) {

                $locationJSON = $request->request->get('location');
                $location = json_decode($locationJSON);

                $placeHandler = $this->container->get('place.handler.place');

                $placeHandler->create($user, $place, $step, $location);

                return $this->redirectToRoute('travel_show_step', array(
                    'slug' => $travel->getSlug(),
                    'stepSlug' => $step->getSlug(),
                    'stepId' => $step->getId()
                ));
            }
        }

        return $this->render('PlaceBundle:Place:add.html.twig', array(
            'form' => $form->createView(),
            'travel' => $travel,
            'step' => $step,
            'types' => $types,
            'user' => $user,
            'isOwner' => $isOwner
        ));
    }

    /**
     * @Route("/place/{slug}/{placeId}", name="place_show")
     *
     * @param int $placeId
     */
    public function showAction($placeId)
    {
        $placeHandler = $this->container->get('place.handler.place');

        $place = $placeHandler->getById($placeId);

        if (null == $place) {
            throw new NotFoundHttpException('Place Not Found!');
        }

        $user = $this->getUser();

        return $this->render('PlaceBundle:Place:show.html.twig', array(
            'place' => $place,
            'user' => $user
        ));
    }

    /**
     * @Route("/travel/{stepSlug}/edit-places/{stepId}", name="travel_edit_places", requirements={"stepId" = "\d+"})
     *
     * @param $stepId
     */
    public function editPlacesAction($stepId)
    {
        $stepHandler = $this->container->get('travel.handler.step');

        $step = $stepHandler->getById($stepId);

        if (null == $step) {
            throw new NotFoundHttpException('Step Not Found!');
        }

        $travel = $step->getTravel();

        $user = $this->getUser();

        if (null == $travel || null == $user) {
            throw new NotFoundHttpException('Travel Not Found');
        }

        $travelHandler = $this->container->get('travel.handler.travel');

        $isOwner = $travelHandler->isOwner($user, $travel);

        if (!$isOwner) {
            throw new AccessDeniedException('Permission Denied!');
        }

        $request = $this->container->get('request');

        if($request->getMethod() == 'POST') {

            $itemOrders = $request->request->get('order');

            foreach ($step->getPlaceItems() as $placeItem) {
                foreach ($itemOrders as $key => $val) {
                    if ($placeItem->getId() == $key) {
                        $placeItem->setItemOrder((int) $val);
                    }
                }
            }

            $travelHandler->save($travel);

            return $this->redirectToRoute('travel_edit_places', array(
                'stepSlug' => $step->getSlug(),
                'stepId' => $step->getId()
            ));
        }

        return $this->render('PlaceBundle:Place:edit-step-places.html.twig', array(
            'travel' => $travel,
            'step' => $step,
            'user' => $user,
            'isOwner' => $isOwner
        ));
    }

    /**
     * @Route(
     *     "/travel/place/delete-step-place/{placeItemId}",
     *     name="travel_delete_place_item",
     *     requirements={"placeItemId" = "\d+"},
     *     options={"expose"=true}
     * )
     *
     * @param $placeItemId
     */
    public function deletePlaceItemAction($placeItemId)
    {
        $placeItemHandler = $this->container->get('place.handler.place_item');

        $placeItem = $placeItemHandler->getById($placeItemId);

        if (null == $placeItem) {
            throw new NotFoundHttpException('Place Not Found!');
        }

        $step = $placeItem->getStep();

        $travel = $step->getTravel();

        $user = $this->getUser();

        if (null == $travel || null == $user) {
            throw new NotFoundHttpException('Travel Not Found');
        }

        $travelHandler = $this->container->get('travel.handler.travel');

        $isOwner = $travelHandler->isOwner($user, $travel);

        if (!$isOwner) {
            throw new AccessDeniedException('Permission Denied!');
        }

        $placeItemHandler->remove($placeItem);

        return $this->redirectToRoute('travel_edit_places', array(
            'stepSlug' => $step->getSlug(),
            'stepId' => $step->getId()
        ));
    }
}

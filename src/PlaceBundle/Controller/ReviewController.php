<?php

namespace PlaceBundle\Controller;

use PlaceBundle\Entity\Place;
use PlaceBundle\Entity\Review;
use PlaceBundle\Form\PlaceType;
use PlaceBundle\Form\ReviewType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class ReviewController extends Controller
{
    /**
     * @Route("/place/{slug}/{placeId}/add-review", name="place_add_review")
     *
     * @param int $placeId
     */
    public function addAction($placeId)
    {
        $placeHandler = $this->container->get('place.handler.place');

        $place = $placeHandler->getById($placeId);

        if (null == $place) {
            throw new NotFoundHttpException('Place Not Found!');
        }

        $user = $this->getUser();

        if (null == $user) {
            return $this->redirectToRoute('sonata_user_security_login');
        }

        $review = New Review();

        $form = $this->createForm(new ReviewType(), $review);

        $request = $this->container->get('request');

        if($request->getMethod() == 'POST') {
            $form->bind($request);

            if ($form->isValid()) {
                $review->setPlace($place);
                $review->setCreator($user);
                $reviewHandler = $this->get('place.handler.review');

                $reviewHandler->save($review);

                return $this->redirectToRoute('place_show', array('slug' => $place->getSlug(), 'placeId' => $placeId));
            }
        }

        return $this->render('PlaceBundle:Review:add-review.html.twig', array(
            'place' => $place,
            'form' => $form->createView()
        ));
    }


    /**
     * @Route("/place/review/delete-review/{reviewId}", name="place_delete_review", options={"expose"=true})
     *
     * @param int $reviewId
     */
    public function deleteAction($reviewId)
    {
        $reviewHandler = $this->get('place.handler.review');

        $review = $reviewHandler->getById($reviewId);

        $user = $this->getUser();

        if (null == $review || null == $user) {
            throw new NotFoundHttpException('Review Not Found!');
        }

        $place = $review->getPlace();

        if (!$reviewHandler->isOwner($user, $review)) {
            throw new NotFoundHttpException('Permission Denied!');
        }

        $reviewHandler->remove($review);

        return $this->redirectToRoute('place_show', array('slug' => $place->getSlug(), 'placeId' => $place->getId()));
    }
}

<?php

namespace PlaceBundle\Entity;

use AppBundle\Traits\CreatedUpdatedTrait;
use AppBundle\Traits\SlugTrait;
use Application\Sonata\MediaBundle\Entity\Media;
use Application\Sonata\UserBundle\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use PointBundle\Entity\Point;

/**
 * Place
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="PlaceBundle\Entity\PlaceRepository")
 */
class Place
{
    use CreatedUpdatedTrait;
    use SlugTrait;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="place_map_id", type="string")
     */
    private $placeMapId;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string", length=255, nullable=true)
     */
    private $address;

    /**
     * @var float
     *
     * @ORM\Column(name="latitude", type="float")
     */
    private $latitude;

    /**
     * @var float
     *
     * @ORM\Column(name="longitude", type="float")
     */
    private $longitude;

    /**
     * @var int
     *
     * @ORM\Column(name="popularity", type="integer")
     */
    private $popularity = 0;

    /**
     * @var string json
     *
     * @ORM\Column(name="type_string", type="string", length=255, nullable=true)
     */
    private $typeString;

    /**
     * @var Review[]
     *
     * @ORM\OneToMany(targetEntity="PlaceBundle\Entity\Review", mappedBy="place", cascade={"persist", "remove"})
     * @ORM\OrderBy({"createdAt" = "DESC"})
     */
    private $reviews;

    /**
     * @var Type[]
     *
     * @ORM\ManyToMany(targetEntity="PlaceBundle\Entity\Type")
     * @ORM\OrderBy({"name" = "ASC"})
     */
    private $types;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="Application\Sonata\UserBundle\Entity\User")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    private $creator;

    /**
     * @var Media
     *
     * @ORM\OneToOne(targetEntity="Application\Sonata\MediaBundle\Entity\Media", cascade={"persist", "remove"})
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    private $image;

    /**
     * @var Point
     * @ORM\ManyToOne(targetEntity="PointBundle\Entity\Point")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $point;


    public function __construct()
    {
        $this->reviews = new ArrayCollection();
        $this->types = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getPlaceMapId()
    {
        return $this->placeMapId;
    }

    /**
     * @param string $placeMapId
     * @return Place
     */
    public function setPlaceMapId($placeMapId)
    {
        $this->placeMapId = $placeMapId;

        return $this;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Place
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param string $address
     * @return Place
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Set latitude
     *
     * @param float $latitude
     * @return Place
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;

        return $this;
    }

    /**
     * Get latitude
     *
     * @return float
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * Set longitude
     *
     * @param float $longitude
     * @return Place
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;

        return $this;
    }

    /**
     * Get longitude
     *
     * @return float
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * @return int
     */
    public function getPopularity()
    {
        return $this->popularity;
    }

    /**
     * @param int $popularity
     * @return Place
     */
    public function setPopularity($popularity)
    {
        $this->popularity = $popularity;

        return $this;
    }

    /**
     * Set image
     *
     * @param Media $image
     * @return Place
     */
    public function setImage(Media $image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return Media
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @return string
     */
    public function getTypeString()
    {
        return $this->typeString;
    }

    /**
     * @param string $typeString
     * @return Place
     */
    public function setTypeString($typeString)
    {
        $this->typeString = $typeString;

        return $this;
    }

    /**
     * @param Review $review
     * @return Place
     */
    public function addReview(Review $review)
    {
        $this->reviews[] = $review;

        $review->setPlace($this);

        return $this;
    }

    /**
     * @param Review $review
     * @return Place
     */
    public function removeReview(Review $review)
    {
        $this->reviews->removeElement($review);

        return $this;
    }

    /**
     * @return ArrayCollection|Collection
     */
    public function getReviews()
    {
        return $this->reviews;
    }

    /**
     * @param Type $type
     * @return Place
     */
    public function addType(Type $type)
    {
        $this->types[] = $type;

        return $this;
    }

    /**
     * @param Type $type
     * @return Place
     */
    public function removeType(Type $type)
    {
        $this->types->removeElement($type);

        return $this;
    }

    /**
     * @return Type[]
     */
    public function getTypes()
    {
        return $this->types;
    }

    /**
     * @return User
     */
    public function getCreator()
    {
        return $this->creator;
    }

    /**
     * @param User $creator
     *
     * @return $this
     */
    public function setCreator($creator)
    {
        $this->creator = $creator;

        return $this;
    }

    /**
     * @return Point
     */
    public function getPoint()
    {
        return $this->point;
    }

    /**
     * @param Point $point
     * @return Place
     */
    public function setPoint($point)
    {
        $this->point = $point;

        return $this;
    }
}

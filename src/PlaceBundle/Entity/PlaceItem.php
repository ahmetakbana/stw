<?php

namespace PlaceBundle\Entity;

use AppBundle\Traits\CreatorTrait;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use TravelBundle\Entity\Step;

/**
 * PlaceItem
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="PlaceBundle\Entity\PlaceItemRepository")
 */
class PlaceItem
{
    use CreatorTrait;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="visitTime", type="datetime", nullable=true)
     */
    private $visitTime;

    /**
     * @var int
     *
     * @ORM\Column(name="item_order", type="integer")
     */
    private $itemOrder = 0;

    /**
     * @var Collection
     *
     * @ORM\ManyToOne(targetEntity="TravelBundle\Entity\Step", inversedBy="placeItems", cascade={"persist"})
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $step;

    /**
     * @var Place
     *
     * @ORM\ManyToOne(targetEntity="PlaceBundle\Entity\Place")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $place;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set visitTime
     *
     * @param \DateTime $visitTime
     * @return PlaceItem
     */
    public function setVisitTime($visitTime)
    {
        $this->visitTime = $visitTime;

        return $this;
    }

    /**
     * Get visitTime
     *
     * @return \DateTime 
     */
    public function getVisitTime()
    {
        return $this->visitTime;
    }

    /**
     * @return int
     */
    public function getItemOrder()
    {
        return $this->itemOrder;
    }

    /**
     * @param int $itemOrder
     * @return PlaceItem
     */
    public function setItemOrder($itemOrder)
    {
        $this->itemOrder = $itemOrder;

        return $this;
    }

    /**
     * @param Step $step
     * @return PlaceItem
     */
    public function setStep(Step $step)
    {
        $this->step = $step;

        return $this;
    }

    /**
     * @return Step
     */
    public function getStep()
    {
        return $this->step;
    }

    /**
     * @param Place $place
     * @return PlaceItem
     */
    public function setPlace(Place $place)
    {
        $this->place = $place;

        return $this;
    }

    /**
     * @return Place
     */
    public function getPlace()
    {
        return $this->place;
    }
}

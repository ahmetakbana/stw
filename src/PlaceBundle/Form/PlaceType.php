<?php

namespace PlaceBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class PlaceType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, Array $options)
    {
        $builder
            ->add('image', 'hidden')
        ;
    }

    public function getName()
    {
        return 'travel_place';
    }
}
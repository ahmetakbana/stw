<?php

namespace PlaceBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use PlaceBundle\Entity\Type;

class LoadTypeData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $type = new Type();
        $type->setType('art_gallery');
        $type->setName('Art Gallery');
        $manager->persist($type);

        $type = new Type();
        $type->setType('bar');
        $type->setName('Bar');
        $manager->persist($type);

        $type = new Type();
        $type->setType('cemetery');
        $type->setName('Cemetery');
        $manager->persist($type);

        $type = new Type();
        $type->setType('church');
        $type->setName('Church');
        $manager->persist($type);

        $type = new Type();
        $type->setType('cafe');
        $type->setName('Cafe');
        $manager->persist($type);

        $type = new Type();
        $type->setType('library');
        $type->setName('Library');
        $manager->persist($type);

        $type = new Type();
        $type->setType('mosque');
        $type->setName('Mosque');
        $manager->persist($type);

        $type = new Type();
        $type->setType('museum');
        $type->setName('Museum');
        $manager->persist($type);

        $type = new Type();
        $type->setType('night_club');
        $type->setName('Night Club');
        $manager->persist($type);

        $type = new Type();
        $type->setType('park');
        $type->setName('Park');
        $manager->persist($type);

        $type = new Type();
        $type->setType('restaurant');
        $type->setName('Restaurant');
        $manager->persist($type);

        $type = new Type();
        $type->setType('shopping_mall');
        $type->setName('Shopping Mall');
        $manager->persist($type);

        $type = new Type();
        $type->setType('spa');
        $type->setName('Spa');
        $manager->persist($type);

        $type = new Type();
        $type->setType('store');
        $type->setName('Store');
        $manager->persist($type);

        $type = new Type();
        $type->setType('synagogue');
        $type->setName('Synagogue');
        $manager->persist($type);

        $type = new Type();
        $type->setType('zoo');
        $type->setName('Zoo');
        $manager->persist($type);

        $manager->flush();
    }

    public function getOrder()
    {
        return 200;
    }
}
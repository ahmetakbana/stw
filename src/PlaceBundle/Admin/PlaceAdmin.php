<?php

namespace PlaceBundle\Admin;

use PlaceBundle\Entity\Place;
use PlaceBundle\Entity\PlaceItemRepository;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ProxyQueryInterface;
use Sonata\AdminBundle\Form\FormMapper;

class PlaceAdmin extends Admin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->tab('General')
                ->with('General')
                    ->add('name')
                    ->add('address')
                ->end()
            ->end()
            ->tab('Image')
                ->with('Image')
                    ->add('image', 'sonata_media_type', array(
                        'provider' => 'sonata.media.provider.image',
                        'context' => 'place',
                    ))
                ->end()
            ->end()
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('name')
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('name')
            ->add('creator')
            ->add('_action', null, array(
                'actions' => array(
                    'delete' => array(),
                    'preview' => array(
                        'template' => 'PlaceBundle:Admin:list__action_preview_travel.html.twig'
                    )
                )
            ))
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function preRemove($place)
    {
        $this->removeTimeLine($place);
        parent::preRemove($place);
    }

    /**
     * {@inheritdoc}
     */
    public function preBatchAction($actionName, ProxyQueryInterface $query, array &$idx, $allElements)
    {
        if ('delete' == $actionName) {
            $container = $this->getConfigurationPool()->getContainer();
            $placeRepository = $container->get('place.repository.place');

            foreach ($idx as $placeId) {
                $place = $placeRepository->findOneBy(array('id' => $placeId));
                $this->removeTimeLine($place);
            }
        }
    }

    /**
     * @param Place $place
     */
    protected function removeTimeLine(Place $place)
    {
        $container = $this->getConfigurationPool()->getContainer();

        /** @var PlaceItemRepository $placeItemRepository */
        $placeItemRepository = $container->get('place.repository.place_item');

        $placeItems = $placeItemRepository->getByPlace($place);

        $timeLineHandler = $container->get('time_line.handler');

        foreach ($placeItems as $placeItem) {
            $timeLineHandler->userRemovePlace($placeItem);
        }
    }
}
<?php

namespace Application\Sonata\UserBundle\Entity;

use Doctrine\ORM\EntityRepository;

class UserRepository extends EntityRepository
{
    public function getPublic()
    {
        return $this->createQueryBuilder('user')
            ->where('user.locked != true')
            ->andWhere('user.expired != true')
            ->andWhere('user.roles = :roles')
            ->setParameter('roles', serialize(array('ROLE_USER')))
            ->getQuery()
            ->useResultCache(true)
            ->getResult()
        ;
    }
}
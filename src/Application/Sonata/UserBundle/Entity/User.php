<?php

namespace Application\Sonata\UserBundle\Entity;

use Application\Sonata\MediaBundle\Entity\Media;
use Doctrine\Common\Collections\ArrayCollection;
use Sonata\UserBundle\Entity\BaseUser as BaseUser;
use TravelBundle\Entity\Travel;

class User extends BaseUser
{
    /**
     * @var int $id
     */
    protected $id;

    /**
     * @var Media
     */
    private $image;

    /**
     * @var Travel[]
     */
    private $travels;


    public function __construct()
    {
        parent::__construct();

        $this->travels = new ArrayCollection();
        $this->roles = array('ROLE_USER');
    }

    /**
     * Get id
     *
     * @return int $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Media
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param Media $image
     * @return User
     */
    public function setImage(Media $image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Add travel
     *
     * @param Travel $travel
     * @return User
     */
    public function addTravel(Travel $travel)
    {
        $this->travels[] = $travel;

        $travel->setUser($this);

        return $this;
    }

    /**
     * Remove travel
     *
     * @param Travel $travel
     * @return User
     */
    public function removeTravel(Travel $travel)
    {
        $this->travels->removeElement($travel);

        return $this;
    }

    /**
     * Get travels
     *
     * @return Travel[]
     */
    public function getTravels()
    {
        return $this->travels;
    }
}

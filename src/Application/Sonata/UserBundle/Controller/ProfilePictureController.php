<?php

namespace Application\Sonata\UserBundle\Controller;

use Application\Sonata\UserBundle\Form\Type\ProfilePictureType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints\File;

class ProfilePictureController extends Controller
{
    /**
     * @Route("/profile-picture", name="sonata_user_profile_picture")
     */
    public function pictureSettingsAction(Request $request)
    {
        $user = $this->get("security.token_storage")->getToken()->getUser();

        $formBuilder = $this->createFormBuilder($user);
        /*
        $formBuilder->add('image', 'sonata_media_type', array(
            'provider' => 'sonata.media.provider.image',
            'context'  => 'user',
            'label' => false
        ));
         */
        $formBuilder->add('image', 'file', array(
            'label' => 'Profile Image',
            'mapped' => false,
            'constraints' => [
                new File([
                    'maxSize' => '1M',
                    'mimeTypes' => ['image/pjpeg','image/jpeg','image/png','image/x-png', 'image/gif'],
                    'mimeTypesMessage' => 'Please upload a valid Picture (Smaller than 1Mb, jpg or png)',
                ])
            ]
        ));

        $form = $formBuilder->getForm();

        if($request->getMethod() == 'POST'){
            $form->handleRequest($request);

            if ($form->isValid()) {

                $media = $user->getImage();
                $mediaHandler = $this->get('app.handler.media_handler');
                if (null !== $media) {
                    $mediaHandler->removeMedia($media);
                }

                $files = $request->files->get('form');
                $file = $files['image'];
                $em = $this->getDoctrine()->getEntityManager();

                $newImage = $mediaHandler->createUserImage($file, $em);

                $user->setImage($newImage);

                $em->persist($user);
                $em->flush();

                return $this->redirectToRoute('sonata_user_profile_show', array('username' => $user->getUserName()));
            }
        }

        return $this->render('ApplicationSonataUserBundle:Profile:profile-picture.html.twig', array(
            'user' => $user,
            'form' => $form->createView()
        ));
    }
}



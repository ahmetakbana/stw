<?php

namespace Application\Sonata\UserBundle\Controller;

use FOS\UserBundle\Model\UserInterface;
use Sonata\UserBundle\Controller\RegistrationFOSUser1Controller as BaseRegistrationController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class RegistrationFOSUser1Controller extends BaseRegistrationController
{
    /**
     * @return RedirectResponse
     */
    public function registerAction()
    {
        $user = $this->get('security.context')->getToken()->getUser();

        if ($user instanceof UserInterface) {
            $this->get('session')->getFlashBag()->set('sonata_user_error', 'sonata_user_already_authenticated');

            return $this->redirectToRoute('sonata_user_profile_show', array('username' => $user->getUsername()));
        }

        $form = $this->get('sonata.user.registration.form');
        $formHandler = $this->get('sonata.user.registration.form.handler');
        $confirmationEnabled = $this->container->getParameter('fos_user.registration.confirmation.enabled');

        $username = $this->getRequest()->request->get('sonata_user_registration_form')['username'];

        $deniedUsername = array(
            'contact',
            'what-is-it',
            'terms-of-service',
            'privacy-policy',
            'travel',
            'place',
            'step',
            'profile',
            'login',
            'logout',
            'resetting',
            'register',
            'admin',
            'help',
            'search',
            'sitemap.xml',
            'robots.txt'
        );

        if (!in_array($username, $deniedUsername)) {
            $process = $formHandler->process($confirmationEnabled);
            if ($process) {
                $user = $form->getData();

                $authUser = false;
                if ($confirmationEnabled) {
                    $this->get('session')->set('fos_user_send_confirmation_email/email', $user->getEmail());
                    $url = $this->generateUrl('fos_user_registration_check_email');
                } else {
                    $authUser = true;
                    $route = $this->get('session')->get('sonata_basket_delivery_redirect');

                    if (null !== $route) {
                        $this->get('session')->remove('sonata_basket_delivery_redirect');
                        $url = $this->generateUrl($route);
                    } else {
                        $url = $this->get('session')->get('sonata_user_redirect_url');
                    }
                }

                $this->setFlash('fos_user_success', 'registration.flash.user_created');

                $response = new RedirectResponse($url);

                if ($authUser) {
                    $this->authenticateUser($user, $response);
                }

                return $response;
            }
        }

        $this->get('session')->set('sonata_user_redirect_url', $this->get('request')->headers->get('referer'));

        return $this->render('FOSUserBundle:Registration:register.html.'.$this->getEngine(), array(
            'form' => $form->createView(),
        ));
    }
}

<?php

namespace Application\Sonata\UserBundle\Security\Core\User;

use FOS\UserBundle\Model\UserManagerInterface;
use HWI\Bundle\OAuthBundle\OAuth\Response\UserResponseInterface;
use HWI\Bundle\OAuthBundle\Security\Core\User\FOSUBUserProvider as BaseProvider;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class FOSUBUserProvider extends BaseProvider
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @param UserManagerInterface $userManager
     * @param array $properties
     * @param ContainerInterface $container
     */
    public function __construct(
        UserManagerInterface $userManager,
        array $properties,
        ContainerInterface $container
    )
    {
        parent::__construct($userManager, $properties);

        $this->container = $container;
    }

    /**
     * {@inheritDoc}
     */
    public function connect(UserInterface $user, UserResponseInterface $response)
    {
        $property = $this->getProperty($response);
        $username = $response->getUsername();

        //on connect - get the access token and the user ID
        $service = $response->getResourceOwner()->getName();
        $setter = 'set'.ucfirst($service);
        $setter_id = $setter.'UId';
        $setter_token = $setter.'Data';

        //we "disconnect" previously connected users
        if (null !== $previousUser = $this->userManager->findUserBy(array($property => $username))) {
            $previousUser->$setter_id(null);
            $previousUser->$setter_token(null);
            $this->userManager->updateUser($previousUser);
        }

        //we connect current user
        $user->$setter_id($username);
        $user->$setter_token($response->getAccessToken());
        $this->userManager->updateUser($user);
    }

    /**
     * {@inheritdoc}
     */
    public function loadUserByOAuthUserResponse(UserResponseInterface $response)
    {
        $username = $response->getUsername();
        $user = $this->userManager->findUserBy(array($this->getProperty($response) => $username));

        //when the user is registrating
        if (null === $user) {
            $service = $response->getResourceOwner()->getName();
            $setter = 'set'.ucfirst($service);
            $setter_id = $setter.'UId';
            $setter_token = $setter.'Data';

            // create new user here
            $user = $this->userManager->createUser();
            $user->$setter_id($username);
            $user->$setter_token($response->getAccessToken());

            $realName = $response->getFirstName().' '.$response->getLastName();

            $user->setFirstName($realName);

            $newUserName = str_replace(' ', '_', strtolower($realName));

            $checkUserEmail = 1;
            while (0 != $checkUserEmail) {
                if (null == $this->userManager->findUserBy(array('username' => $newUserName))) {
                    $user->setUsername($newUserName);
                    break;
                }
                $newUserName .= $checkUserEmail;
                $checkUserEmail++;
            }

            if (null == $response->getEmail()) {
                $this->container->get('session')->getFlashBag()->add(
                    'social-login-error',
                    'Email permission is required to register you to the web site. Please try connect again!'
                );

                return $user;
            }

            if (null == $this->userManager->findUserBy(array('email' =>  $response->getEmail()))) {
                $user->setEmail($response->getEmail());
                $pwd = bin2hex(openssl_random_pseudo_bytes(4));
                $user->setPlainPassword($pwd);

                $message = \Swift_Message::newInstance()
                    ->setSubject('Step The World - Password')
                    ->setFrom('no-reply@steptheworld.com')
                    ->setTo($response->getEmail())
                    ->setBody(
                        $this->container->get('templating')
                            ->render('ApplicationSonataUserBundle:Registration:passwordEmail.html.twig', array(
                                'user' => $user,
                                'pwd' => $pwd
                            ))
                    );
                $this->container->get('mailer')->send($message);

                $this->container->get('session')->getFlashBag()->add(
                    'social-login',
                    'You have registered and logged in. Your new password and username have been sent to your email. Please update your password. Thank you.'
                );
            } else {
                $user->setEmail($username);
                $user->setPassword($username);
            }

            //I have set all requested data with the user's username
            //modify here with relevant data
            $user->setEnabled(true);
            $this->userManager->updateUser($user);

            return $user;
        }

        //if user exists - go with the HWIOAuth way
        $user = parent::loadUserByOAuthUserResponse($response);
        $serviceName = $response->getResourceOwner()->getName();
        $setter = 'set' . ucfirst($serviceName) . 'Data';

        //update access token
        $user->$setter($response->getAccessToken());
        return $user;
    }
}
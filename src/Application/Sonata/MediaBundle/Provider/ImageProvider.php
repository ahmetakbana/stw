<?php

namespace Application\Sonata\MediaBundle\Provider;

use Sonata\MediaBundle\Model\MediaInterface;
use Sonata\MediaBundle\Provider\ImageProvider as SonataImageProvider;

class ImageProvider extends SonataImageProvider
{
    protected function doTransform(MediaInterface $media)
    {
        // Check if the entity is new
        if (!is_null($media->getId())) {
            // Prevent the file from being erased
            $media->resetBinaryContent();
            // Prevent the provider reference from being erased
            $media->setProviderReference($media->getPreviousProviderReference());
            return;
        }

        // If new media, nothing special to do
        return parent::doTransform($media);
    }
}


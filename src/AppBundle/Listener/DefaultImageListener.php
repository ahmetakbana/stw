<?php

namespace AppBundle\Listener;

use AppBundle\Handler\MediaHandler;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Sonata\UserBundle\Model\UserInterface;

class DefaultImageListener
{
    /**
     * @var MediaHandler
     */
    private $mediaHandler;

    /**
     * @param MediaHandler $mediaHandler
     */
    public function __construct(MediaHandler $mediaHandler)
    {
        $this->mediaHandler = $mediaHandler;
    }

    public function prePersist(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();

        if ($entity instanceof UserInterface) {
            if (null === $entity->getImage()) {
                $entityManager = $args->getEntityManager();
                $media = $this->mediaHandler->createDefaultAvatar($entityManager);
                $entity->setImage($media);
            }
        }
    }
}
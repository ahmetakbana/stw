<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Enquiry;
use AppBundle\Form\EnquiryType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class PageController extends Controller
{
    /**
     * @Route("/", name="app_homepage")
     */
    public function indexAction()
    {
        return $this->render('AppBundle:Page:draw-new.html.twig', array());
    }

    /**
     * @Route("/contact", name="app_contact")
     */
    public function contactAction(Request $request)
    {
        $enquiry = new Enquiry();
        $form = $this->createForm(new EnquiryType(), $enquiry);

        if($request->getMethod()== 'POST'){
            $form->bind($request);

            if($form->isValid()){

                $message = \Swift_Message::newInstance()
                    ->setSubject('Step The World Contact')
                    ->setFrom('no-reply@steptheworld.com')
                    ->setTo($this->container->getParameter('app.email.contact_email'))
                    ->setBody($this->renderView('AppBundle:Page:contactEmail.html.twig', array('enquiry' => $enquiry)));
                $this->get('mailer')->send($message);

                $this->get('session')->getFlashBag()->add('contact-notice', 'Your message was successfully sent. Thank you!');

                return $this->redirect($this->generateUrl('app_contact'));
            }
        }

        return $this->render('AppBundle:Page:contact.html.twig', array(
            'form'=>$form->CreateView()
        ));
    }

    /**
     * @Route("/what-is-it", name="app_what_is")
     */
    public function whatIsAction()
    {
        return $this->render('AppBundle:Page:whatis.html.twig', array());
    }

    /**
     * @Route("/terms-of-service", name="app_terms_of_service")
     */
    public function termsOfServiceAction()
    {
        return $this->render('AppBundle:Page:terms-of-service.html.twig', array());
    }
}

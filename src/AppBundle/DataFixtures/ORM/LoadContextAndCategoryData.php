<?php

namespace AppBundle\DataFixtures\ORM;

use Application\Sonata\ClassificationBundle\Entity\Category;
use Application\Sonata\ClassificationBundle\Entity\Context;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadContextAndCategoryData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $context = new Context();
        $context->setId('default');
        $context->setName('default');
        $context->setEnabled(true);
        $manager->persist($context);

        $category = new Category();
        $category->setName('default');
        $category->setContext($context);
        $category->setEnabled(true);
        $manager->persist($category);

        $context = new Context();
        $context->setId('user');
        $context->setName('user');
        $context->setEnabled(true);
        $manager->persist($context);

        $category = new Category();
        $category->setName('user');
        $category->setContext($context);
        $category->setEnabled(true);
        $manager->persist($category);

        $context = new Context();
        $context->setId('place');
        $context->setName('place');
        $context->setEnabled(true);
        $manager->persist($context);

        $category = new Category();
        $category->setName('place');
        $category->setContext($context);
        $category->setEnabled(true);
        $manager->persist($category);

        $context = new Context();
        $context->setId('point');
        $context->setName('point');
        $context->setEnabled(true);
        $manager->persist($context);

        $category = new Category();
        $category->setName('point');
        $category->setContext($context);
        $category->setEnabled(true);
        $manager->persist($category);

        $manager->flush();
    }

    public function getOrder()
    {
        return 300;
    }
}

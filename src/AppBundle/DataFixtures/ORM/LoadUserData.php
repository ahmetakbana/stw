<?php

namespace AppBundle\DataFixtures\ORM;

use Application\Sonata\UserBundle\Entity\User;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadUserData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $userAdmin = new User();
        $userAdmin->setFirstname('Admin');
        $userAdmin->setLastname('Admin');
        $userAdmin->setUsername('admin');
        $userAdmin->setEmail('admin@gmail.com');
        $userAdmin->setPlainPassword('admin');
        $userAdmin->setEnabled(true);
        $userAdmin->setRoles(array('ROLE_SUPER_ADMIN'));
        $manager->persist($userAdmin);

        $user = new User();
        $user->setFirstname('John');
        $user->setLastname('Doe');
        $user->setUsername('john');
        $user->setEmail('john@gmail.com');
        $user->setPlainPassword('doe');
        $user->setEnabled(true);
        $manager->persist($user);

        $manager->flush();
    }

    public function getOrder()
    {
        return 1000;
    }
}
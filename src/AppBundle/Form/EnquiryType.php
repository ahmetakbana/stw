<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class EnquiryType extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, Array $options)
	{
		$builder->add('name');
		$builder->add('email','email');
		$builder->add('message','textarea');
	}
	
	public function getName()
	{
		return 'app_contact';
	}
}

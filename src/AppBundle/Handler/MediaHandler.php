<?php

namespace AppBundle\Handler;

use Application\Sonata\MediaBundle\Entity\Media;
use Doctrine\ORM\EntityManager;
use Sonata\MediaBundle\Entity\MediaManager;
use Symfony\Component\HttpFoundation\File\File;

class MediaHandler
{
    /**
     * @var MediaManager
     */
    private $mediaManager;

    /**
     * @var string
     */
    private $rootDir;

    /**
     * @param MediaManager $mediaManager
     * @param $rootDir
     */
    public function __construct(MediaManager $mediaManager, $rootDir)
    {
        $this->mediaManager = $mediaManager;
        $this->rootDir = $rootDir;
    }

    /**
     * @param EntityManager $entityManager
     * @return Media
     */
    public function createDefaultAvatar(EntityManager $entityManager)
    {
        $context = $entityManager->getRepository('ApplicationSonataClassificationBundle:Context')->find('user');
        $category = $entityManager->getRepository('ApplicationSonataClassificationBundle:Category')
            ->findOneBy(array('context' => $context->getId()));

        $file = new File($this->rootDir.'/../web/defaultimages/profilepic/default-avatar.jpg');

        return $this->getMediaByFile($file, $category, $context);
    }

    /**
     * @param File          $file
     * @param EntityManager $entityManager
     * @return Media
     */
    public function createUserImage(File $file, EntityManager $entityManager)
    {
        $context = $entityManager->getRepository('ApplicationSonataClassificationBundle:Context')->find('user');
        $category = $entityManager->getRepository('ApplicationSonataClassificationBundle:Category')
            ->findOneBy(array('context' => $context->getId()));

        return $this->getMediaByFile($file, $category, $context);
    }

    public function removeMedia(Media $media)
    {
        $this->mediaManager->delete($media);
    }

    /**
     * @param string        $url
     * @param EntityManager $entityManager
     * @param string        $context
     * @return Media
     */
    public function createFromUrl($url, EntityManager $entityManager, $context)
    {
        $imageName = basename($url);

        $imageDir = $this->rootDir.'/../web/temp/'.$imageName.'.jpg';

        file_put_contents($imageDir, file_get_contents($url));

        $file = new File($imageDir);

        $context = $entityManager->getRepository('ApplicationSonataClassificationBundle:Context')->find($context);
        $category = $entityManager->getRepository('ApplicationSonataClassificationBundle:Category')
            ->findOneBy(array('context' => $context->getId()));

        $media = $this->getMediaByFile($file, $category, $context);

        @unlink($imageDir);

        return $media;
    }

    /**
     * @param EntityManager $entityManager
     * @return Media
     */
    public function createDefaultPlaceImage(EntityManager $entityManager)
    {
        $context = $entityManager->getRepository('ApplicationSonataClassificationBundle:Context')->find('place');
        $category = $entityManager->getRepository('ApplicationSonataClassificationBundle:Category')
            ->findOneBy(array('context' => $context->getId()));

        $file = new File($this->rootDir.'/../web/defaultimages/placepic/default-place.jpg');

        return $this->getMediaByFile($file, $category, $context);
    }

    /**
     * @param EntityManager $entityManager
     * @return Media
     */
    public function createDefaultPointImage(EntityManager $entityManager)
    {
        $context = $entityManager->getRepository('ApplicationSonataClassificationBundle:Context')->find('point');
        $category = $entityManager->getRepository('ApplicationSonataClassificationBundle:Category')
            ->findOneBy(array('context' => $context->getId()));

        $file = new File($this->rootDir.'/../web/defaultimages/pointpic/default-point.jpg');

        return $this->getMediaByFile($file, $category, $context);
    }

    /**
     * @param File   $file
     * @param string $category
     * @param string $context
     * @param string $provider
     *
     * @return Media
     */
    public function getMediaByFile(File $file, $category, $context, $provider = 'sonata.media.provider.image')
    {
        /** @var Media $media */
        $media = $this->mediaManager->create();
        $media->setBinaryContent($file);
        $media->setEnabled(true);
        $media->setName($file->getFilename());
        $media->setContext($context);
        $media->setCategory($category);
        $media->setProviderName($provider);
        $this->mediaManager->save($media);

        return $media;
    }
}
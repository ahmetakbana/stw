<?php

namespace AppBundle\Traits;

use AppBundle\Tool\Tool;

trait SlugTrait
{
    /**
     * @return string
     */
    public function getSlug()
    {
        return Tool::toSlug($this->getName(), true);
    }
}
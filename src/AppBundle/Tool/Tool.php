<?php

namespace AppBundle\Tool;

use Symfony\Component\Filesystem\Filesystem;

class Tool
{
    /**
     * @param $string
     * @param bool $toLowercase
     * @return string
     */
    public static function toSlug($string, $toLowercase = false)
    {
        if ($toLowercase) {
            $string = strtolower($string);
        }

        $string = preg_replace('#[ \-\/\\\+\&]+#', ' ', preg_replace('#[^ \-\/\+\\\&A-Za-z0-9]#', '', $string));
        $string = str_replace(' ', '-', trim($string));

        if ('' === $string) {
            $string = '--';
        }

        return $string;
    }
}
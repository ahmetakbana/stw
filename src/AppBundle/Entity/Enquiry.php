<?php

namespace AppBundle\Entity;

use Symfony\Component\Validator\Mapping\ClassMetadata;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Length;

class Enquiry
{
	/**
	 * Name of Sender
	 * @var string
	 */
	private $name;
	
	/**
	 * Email of Sender
	 * @var string
	 */
	private $email;
	
	/**
	 * Message
	 * @var string
	 */
	private $message;
	
	/**
	 * Gets the name of the sender
	 * @return string $name
	 */
	public function getName()
	{
		return $this->name;
	}
	
	/**
	 * Sets the name of the sender
	 * @param string $name
	 */
	public function setName($name)
	{
		$this->name = $name;
	}
	
	/**
	 * Gets the email address of the sender
	 * @return string $email
	 */
	public function getEmail()
	{
		return $this->email;
	}
	
	/**
	 * Sets the email address of the sender
	 * @param string $email
	 */
	public function setEmail($email)
	{
		$this->email = $email;
	}
	
	/**
	 * Gets the message
	 * @return string $body
	 */
	public function getMessage()
	{
		return $this->message;
	}
	
	/**
	 * Sets the message
	 * @param string $body
	 */
	public function setMessage($message)
	{
		$this->message = $message;
	}
	
	/**
	 * Contact form validator
	 * @param ClassMetadata $metadata
	 */
	public static function loadValidatorMetadata(ClassMetadata $metadata)
	{
		$metadata->addPropertyConstraint('name', new NotBlank());
		$metadata->addPropertyConstraint('email', new Email());
		$metadata->addPropertyConstraint('message', new Length(array('min'=>20)));
		$metadata->addPropertyConstraint('message', new Length(array('max'=>500)));
	}
}

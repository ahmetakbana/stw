<?php

namespace TravelBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use TravelBundle\Entity\StepNote;
use TravelBundle\Form\StepNoteType;

class StepNoteController extends Controller
{
    /**
     * @Route("/travel/{stepId}/add-step-note", name="travel_add_step_note", requirements={"stepId" = "\d+"})
     *
     * @param $stepId
     */
    public function addStepNoteAction($stepId)
    {
        $stepHandler = $this->container->get('travel.handler.step');

        $step = $stepHandler->getById($stepId);

        $travel = $step->getTravel();

        $user = $this->getUser();

        if (null == $travel || null == $user) {
            throw new NotFoundHttpException('Travel Not Found');
        }

        $travelHandler = $this->container->get('travel.handler.travel');

        $isOwner = $travelHandler->isOwner($user, $travel);

        if (!$isOwner) {
            throw new AccessDeniedException('Permission Denied!');
        }

        $stepNote = new StepNote();

        $form = $this->createForm(new StepNoteType(), $stepNote);

        $request = $this->container->get('request');

        if($request->getMethod() == 'POST') {
            $form->bind($request);

            if ($form->isValid()) {

                $stepNoteHandler = $this->container->get('travel.handler.step_note');

                $stepNoteHandler->save($user, $step, $stepNote);

                return $this->redirectToRoute('travel_show_step', array(
                    'stepSlug' => $step->getSlug(),
                    'stepId' => $stepId,
                    'slug' => $travel->getSlug()
                ));
            }
        }

        return $this->render('TravelBundle:StepNote:add-step-note.html.twig', array(
            'form' => $form->createView(),
            'travel' => $travel,
            'step' => $step,
            'user' => $user,
            'isOwner' => $isOwner
        ));
    }

    /**
     * @Route(
     *     "/travel/delete-step-note/{stepNoteId}",
     *     name="travel_delete_step_note",
     *     requirements={"stepNoteId" = "\d+"},
     *     options={"expose"=true}
     * )
     *
     * @param $stepNoteId
     */
    public function deleteStepNoteAction($stepNoteId)
    {
        $stepNoteHandler = $this->container->get('travel.handler.step_note');

        $stepNote = $stepNoteHandler->getById($stepNoteId);

        if (null == $stepNote) {
            throw new NotFoundHttpException('Note Not Found!');
        }

        $step = $stepNote->getStep();

        $travel = $step->getTravel();

        $user = $this->getUser();

        if (null == $travel || null == $user) {
            throw new NotFoundHttpException('Travel Not Found');
        }

        $travelHandler = $this->container->get('travel.handler.travel');

        $isOwner = $travelHandler->isOwner($user, $travel);

        if (!$isOwner) {
            throw new AccessDeniedException('Permission Denied!');
        }

        $stepNoteHandler->remove($stepNote);

        return $this->redirectToRoute('travel_show_step', array(
            'stepSlug' => $step->getSlug(),
            'stepId' => $step->getId(),
            'slug' => $travel->getSlug()
        ));
    }
}
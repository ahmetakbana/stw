<?php

namespace TravelBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use TravelBundle\Entity\Step;
use TravelBundle\Form\StepTimeType;
use TravelBundle\Form\StepType;
use TravelBundle\Form\TravelStepType;

class StepController extends Controller
{
    /**
     * @Route("/travel/{slug}/step/{stepSlug}/{stepId}", name="travel_show_step", requirements={"stepId" = "\d+"})
     *
     * @param $stepId
     */
    public function showStepAction($stepId)
    {
        $stepHandler = $this->container->get('travel.handler.step');

        $step = $stepHandler->getById($stepId);

        if (null == $step) {
            throw new NotFoundHttpException('Step Not Found!');
        }

        $travel = $step->getTravel();

        $user = $this->getUser();

        $travelHandler = $this->container->get('travel.handler.travel');

        $isOwner = false;

        if (null != $user) {
            $isOwner = $travelHandler->isOwner($user, $travel);
        }

        if (!$isOwner && !$travel->isPublic()) {
            throw new AccessDeniedException('Travel Is Private');
        }

        return $this->render('TravelBundle:Step:show-step.html.twig', array(
            'travel' => $travel,
            'step' => $step,
            'user' => $user,
            'isOwner' => $isOwner
        ));
    }

    /**
     * @Route("/travel/set-step-time/{stepId}", name="travel_set_step_time", requirements={"stepId" = "\d+"})
     *
     * @param $stepId
     */
    public function setTimeAction($stepId)
    {
        $stepHandler = $this->container->get('travel.handler.step');

        $step = $stepHandler->getById($stepId);

        if (null == $step) {
            throw new NotFoundHttpException('Step Not Found!');
        }

        $travel = $step->getTravel();

        $user = $this->getUser();

        if (null == $travel || null == $user) {
            throw new NotFoundHttpException('Travel Not Found');
        }

        $travelHandler = $this->container->get('travel.handler.travel');

        $isOwner = $travelHandler->isOwner($user, $travel);

        if (!$isOwner) {
            throw new AccessDeniedException('Permission Denied!');
        }

        $form = $this->createForm(new StepTimeType(), $step);

        $request = $this->container->get('request');

        if($request->getMethod() == 'POST') {
            $form->bind($request);

            if ($form->isValid()) {

                $stepHandler->save($step);

                return $this->redirectToRoute('travel_show_step', array(
                    'slug' => $travel->getSlug(),
                    'stepSlug' => $step->getSlug(),
                    'stepId' => $step->getId()
                ));
            }
        }

        return $this->render('TravelBundle:Step:set-step-time.html.twig', array(
            'form' => $form->createView(),
            'travel' => $travel,
            'step' => $step,
            'user' => $user,
            'isOwner' => $isOwner
        ));
    }

    /**
     * @Route("/travel/add-step/{travelId}", name="travel_add_step", requirements={"travelId" = "\d+"})
     *
     * @param $travelId
     */
    public function addStepAction($travelId)
    {
        $travelHandler = $this->container->get('travel.handler.travel');

        $travel = $travelHandler->getById($travelId);

        $user = $this->getUser();

        if (null == $travel || null == $user) {
            throw new NotFoundHttpException('Travel Not Found');
        }

        $isOwner = $travelHandler->isOwner($user, $travel);

        if (!$isOwner) {
            throw new AccessDeniedException('Permission Denied!');
        }

        $step = new Step();

        $form = $this->createForm(new StepType(), $step);

        $request = $this->container->get('request');

        if($request->getMethod() == 'POST') {
            $form->bind($request);

            if ($form->isValid()) {

                $locationJSON = $request->request->get('location');
                $location = json_decode($locationJSON);

                $stepHandler = $this->container->get('travel.handler.step');

                $step = $stepHandler->create($user, $travel, $location[0]);

                return $this->redirectToRoute('travel_show_step', array(
                    'slug' => $travel->getSlug(),
                    'stepSlug' => $step->getSlug(),
                    'stepId' => $step->getId()
                ));
            }
        }

        return $this->render('TravelBundle:Step:add-step.html.twig', array(
            'form' => $form->createView(),
            'travel' => $travel,
            'user' => $user,
            'isOwner' => $isOwner
        ));
    }

    /**
     * @Route("/travel/edit-steps/{travelId}", name="travel_edit_steps", requirements={"travelId" = "\d+"})
     *
     * @param $travelId
     */
    public function editStepsAction($travelId)
    {
        $travelHandler = $this->container->get('travel.handler.travel');

        $travel = $travelHandler->getById($travelId);

        $user = $this->getUser();

        if (null == $travel || null == $user) {
            throw new NotFoundHttpException('Travel Not Found');
        }

        $isOwner = $travelHandler->isOwner($user, $travel);

        if (!$isOwner) {
            throw new AccessDeniedException('Permission Denied!');
        }

        $request = $this->container->get('request');

        if($request->getMethod() == 'POST') {

            $stepOrders = $request->request->get('order');

            foreach ($travel->getSteps() as $step) {
                foreach ($stepOrders as $key => $val) {
                    if ($step->getId() == $key) {
                        $step->setStepOrder((int) $val);
                    }
                }
            }

            $travelHandler->save($travel);

            return $this->redirectToRoute('travel_edit_steps', array('travelId' => $travel->getId()));
        }

        return $this->render('TravelBundle:Step:edit-steps.html.twig', array(
            'travel' => $travel,
            'user' => $user,
            'isOwner' => $isOwner
        ));
    }

    /**
     * @Route(
     *     "/travel/delete-step/{stepId}",
     *     name="travel_delete_step",
     *     requirements={"stepId" = "\d+"},
     *     options={"expose"=true}
     * )
     *
     * @param $stepId
     */
    public function deleteStepAction($stepId)
    {
        $stepHandler = $this->container->get('travel.handler.step');

        $step = $stepHandler->getById($stepId);

        if (null == $step) {
            throw new NotFoundHttpException('Step Not Found!');
        }

        $travel = $step->getTravel();

        if (count($travel->getSteps()) == 1) {

            $this->addFlash(
                'notice',
                'Travel should have at least one step!'
            );

            return $this->redirectToRoute('travel_edit_steps', array('travelId' => $travel->getId()));
        }

        $user = $this->getUser();

        if (null == $travel || null == $user) {
            throw new NotFoundHttpException('Travel Not Found');
        }

        $travelHandler = $this->container->get('travel.handler.travel');

        $isOwner = $travelHandler->isOwner($user, $travel);

        if (!$isOwner) {
            throw new AccessDeniedException('Permission Denied!');
        }

        $stepHandler->remove($step);

        return $this->redirectToRoute('travel_edit_steps', array('travelId' => $travel->getId()));
    }
}
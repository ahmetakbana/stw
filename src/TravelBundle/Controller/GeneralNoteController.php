<?php

namespace TravelBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use TravelBundle\Entity\GeneralNote;
use TravelBundle\Form\GeneralNoteType;

class GeneralNoteController extends Controller
{
    /**
     * @Route("/travel/{travelId}/add-general-note", name="travel_add_general_note", requirements={"travelId" = "\d+"})
     *
     * @param $travelId
     */
    public function addGeneralNoteAction($travelId)
    {
        $travelHandler = $this->container->get('travel.handler.travel');

        $travel = $travelHandler->getById($travelId);

        $user = $this->getUser();

        if (null == $travel || null == $user) {
            throw new NotFoundHttpException('Travel Not Found');
        }

        $generalNote = new GeneralNote();

        $form = $this->createForm(new GeneralNoteType(), $generalNote);

        $request = $this->container->get('request');

        if($request->getMethod() == 'POST') {
            $form->bind($request);

            if ($form->isValid()) {

                $generalNoteHandler = $this->container->get('travel.handler.general_note');

                $generalNoteHandler->save($user, $travel, $generalNote);

                return $this->redirectToRoute('travel_show', array(
                    'slug' => $travel->getSlug(),
                    'travelId' => $travel->getId()
                ));
            }
        }

        $isOwner = $travelHandler->isOwner($user, $travel);

        if (!$isOwner) {
            throw new AccessDeniedException('Permission Denied!');
        }

        return $this->render('TravelBundle:GeneralNote:add-general-note.html.twig', array(
            'form' => $form->createView(),
            'travel' => $travel,
            'user' => $user,
            'isOwner' => $isOwner
        ));
    }

    /**
     * @Route(
     *     "/travel/delete-general-note/{generalNoteId}",
     *     name="travel_delete_general_note",
     *     requirements={"generalNoteId" = "\d+"},
     *     options={"expose"=true}
     * )
     *
     * @param $generalNoteId
     */
    public function deleteGeneralNoteAction($generalNoteId)
    {
        $generalNoteHandler = $this->container->get('travel.handler.general_note');

        $generalNote = $generalNoteHandler->getById($generalNoteId);

        if (null == $generalNote) {
            throw new NotFoundHttpException('Note Not Found!');
        }

        $travel = $generalNote->getTravel();

        $user = $this->getUser();

        if (null == $travel || null == $user) {
            throw new NotFoundHttpException('Travel Not Found');
        }

        $travelHandler = $this->container->get('travel.handler.travel');

        $isOwner = $travelHandler->isOwner($user, $travel);

        if (!$isOwner) {
            throw new AccessDeniedException('Permission Denied!');
        }

        $generalNoteHandler->remove($generalNote);

        return $this->redirectToRoute('travel_show', array(
            'slug' => $travel->getSlug(),
            'travelId' => $travel->getId()
        ));
    }
}
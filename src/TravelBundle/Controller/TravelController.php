<?php

namespace TravelBundle\Controller;

use Sonata\UserBundle\Model\UserInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\BrowserKit\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use TravelBundle\Entity\Travel;
use TravelBundle\Form\TravelSettingsType;
use TravelBundle\Form\TravelType;

class TravelController extends Controller
{
    /**
     * @Route("/travel/save", name="travel_save", options={"expose"=true})
     */
    public function saveAction()
    {
        $user = $this->get('security.context')->getToken()->getUser();

        if (!is_object($user) || !$user instanceof UserInterface) {
            return $this->redirectToRoute('fos_user_security_login');
        }

        $travel = new Travel();

        $form = $this->createForm(new TravelType(), $travel);

        $request = $this->container->get('request');

        if($request->getMethod() == 'POST') {
            $form->bind($request);

            if ($form->isValid()) {

                $locationsJSON = $request->request->get('locations');
                $locations = json_decode($locationsJSON);

                $user = $this->getUser();

                $travelHandler = $this->container->get('travel.handler.travel');

                $travel = $travelHandler->create($user, $travel, $locations);

                return $this->redirectToRoute('travel_show', array(
                    'slug' => $travel->getSlug(),
                    'travelId' => $travel->getId()
                ));
            }
        }

        return $this->render('TravelBundle:Travel:save.html.twig', array(
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("/travel/{slug}/{travelId}", name="travel_show", requirements={"travelId" = "\d+"})
     *
     * @param $travelId
     */
    public function showAction($travelId)
    {
        $travelHandler = $this->container->get('travel.handler.travel');

        $travel = $travelHandler->getById($travelId);

        if (null == $travel) {
            throw new NotFoundHttpException('Travel Not Found');
        }

        $user = $this->getUser();

        $isOwner = false;

        if (null != $user) {
            $isOwner = $travelHandler->isOwner($user, $travel);
        }

        if (!$isOwner && !$travel->isPublic()) {
            throw new AccessDeniedException('Travel Is Private');
        }

        $timeLine = null;
        if ($travel->isTimeLineActive()) {
            $timeLine = $travelHandler->getTimeLine($travel);
        }

        return $this->render('TravelBundle:Travel:show.html.twig', array(
            'travel' => $travel,
            'user' => $user,
            'isOwner' => $isOwner,
            'timeLine' => $timeLine,
        ));
    }

    /**
     * @Route("/travel/{travelId}/settings", name="travel_settings", requirements={"travelId" = "\d+"})
     *
     * @param $travelId
     */
    public function settingsAction($travelId)
    {
        $travelHandler = $this->container->get('travel.handler.travel');

        $travel = $travelHandler->getById($travelId);

        $user = $this->getUser();

        if (null == $travel || null == $user) {
            throw new NotFoundHttpException('Travel Not Found');
        }

        $isOwner = $travelHandler->isOwner($user, $travel);

        if (!$isOwner) {
            throw new AccessDeniedException('Permission Denied!');
        }

        $form = $this->createForm(new TravelSettingsType(), $travel);

        $request = $this->container->get('request');

        if($request->getMethod() == 'POST') {
            $form->bind($request);

            if ($form->isValid()) {
                $travel = $travelHandler->save($travel);

                return $this->redirectToRoute('travel_show', array(
                    'slug' => $travel->getSlug(),
                    'travelId' => $travel->getId()
                ));
            }
        }

        return $this->render('TravelBundle:Travel:settings.html.twig', array(
            'form' => $form->createView(),
            'travel' => $travel,
            'user' => $user,
            'isOwner' => $isOwner
        ));
    }

    /**
     * @Route("/travel/{travelId}/settings/info", name="travel_settings_info", requirements={"travelId" = "\d+"})
     *
     * @param $travelId
     */
    public function settingsInfoAction($travelId)
    {
        $travelHandler = $this->container->get('travel.handler.travel');

        $travel = $travelHandler->getById($travelId);

        $user = $this->getUser();

        if (null == $travel || null == $user) {
            throw new NotFoundHttpException('Travel Not Found');
        }

        $isOwner = $travelHandler->isOwner($user, $travel);

        if (!$isOwner) {
            throw new AccessDeniedException('Permission Denied!');
        }

        $form = $this->createForm(new TravelType(), $travel);

        $request = $this->container->get('request');

        if($request->getMethod() == 'POST') {
            $form->bind($request);

            if ($form->isValid()) {
                $travel = $travelHandler->save($travel);

                return $this->redirectToRoute('travel_show', array(
                    'slug' => $travel->getSlug(),
                    'travelId' => $travel->getId()
                ));
            }
        }

        return $this->render('TravelBundle:Travel:settings-info.html.twig', array(
            'form' => $form->createView(),
            'travel' => $travel,
            'user' => $user,
            'isOwner' => $isOwner
        ));
    }

    /**
     * @Route(
     *     "/travel/{travelId}/settings/delete",
     *     name="travel_delete_travel",
     *     requirements={"travelId" = "\d+"},
     *     options={"expose"=true}
     * )
     *
     * @param $travelId
     */
    public function deleteAction($travelId)
    {
        $travelHandler = $this->container->get('travel.handler.travel');

        $travel = $travelHandler->getById($travelId);

        $user = $this->getUser();

        if (null == $travel || null == $user) {
            throw new NotFoundHttpException('Travel Not Found');
        }

        $isOwner = $travelHandler->isOwner($user, $travel);

        if (!$isOwner) {
            throw new AccessDeniedException('Permission Denied!');
        }

        $travelHandler->remove($travel);

        return $this->redirectToRoute('sonata_user_profile_show', array('username' => $user->getUsername()));
    }

    /**
     * @Route("/travel/{slug}/{travelId}/map", name="travel_show_map", requirements={"travelId" = "\d+"})
     *
     * @param $travelId
     */
    public function showMapAction($travelId)
    {
        $travelHandler = $this->container->get('travel.handler.travel');

        $travel = $travelHandler->getById($travelId);

        if (null == $travel) {
            throw new NotFoundHttpException('Travel Not Found!');
        }

        $user = $this->getUser();

        $isOwner = false;

        if (null != $user) {
            $isOwner = $travelHandler->isOwner($user, $travel);
        }

        if (!$isOwner && !$travel->isPublic()) {
            throw new AccessDeniedException('Travel Is Private');
        }

        return $this->render('TravelBundle:Travel:show_map.html.twig', array(
            'travel' => $travel,
            'user' => $user,
            'isOwner' => $isOwner
        ));
    }

    /**
     * @Route("/travel/{slug}/{travelId}/timeline", name="travel_show_timeline", requirements={"travelId" = "\d+"})
     *
     * @param $travelId
     */
    public function showTimeLine($travelId)
    {
        $travelHandler = $this->container->get('travel.handler.travel');

        $travel = $travelHandler->getById($travelId);

        if (null == $travel || !$travel->isTimeLineActive()) {
            throw new NotFoundHttpException('Travel Not Found');
        }

        $user = $this->getUser();

        $isOwner = false;

        if (null != $user) {
            $isOwner = $travelHandler->isOwner($user, $travel);
        }

        if (!$isOwner && !$travel->isPublic()) {
            throw new AccessDeniedException('Travel Is Private');
        }

        $timeLine = null;
        if ($travel->isTimeLineActive()) {
            $timeLine = $travelHandler->getTimeLine($travel, 1000);
        }

        return $this->render('TravelBundle:Travel:show-timeline.html.twig', array(
            'travel' => $travel,
            'user' => $user,
            'isOwner' => $isOwner,
            'timeLine' => $timeLine,
        ));
    }

    /**
     * @Route("/travel/{travelId}/like", name="travel_like_travel", requirements={"travelId" = "\d+"})
     *
     * @param $travelId
     */
    public function likeAction($travelId)
    {
        $travelHandler = $this->container->get('travel.handler.travel');

        $travel = $travelHandler->getById($travelId);

        $user = $this->getUser();

        if (null == $user) {
            return new JsonResponse(array('login' => false));
        }

        if (null == $travel) {
            return new JsonResponse(array('success' => false));
        }

        $travelHandler->like($user, $travel);

        return new JsonResponse(array('success' => true));
    }

    /**
     * @Route("/travel/{travelId}/dislike", name="travel_dislike_travel", requirements={"travelId" = "\d+"})
     *
     * @param $travelId
     */
    public function dislikeAction($travelId)
    {
        $travelHandler = $this->container->get('travel.handler.travel');

        $travel = $travelHandler->getById($travelId);

        $user = $this->getUser();

        if (null == $travel || null == $user) {
            return new JsonResponse(array('success' => false));
        }

        $travelHandler->dislike($user, $travel);

        return new JsonResponse(array('success' => true));
    }

    /**
     * @Route("/travel/{slug}/{travelId}/likes", name="travel_show_likes", requirements={"travelId" = "\d+"})
     *
     * @param $travelId
     */
    public function showLikesAction($travelId)
    {
        $travelHandler = $this->container->get('travel.handler.travel');

        $travel = $travelHandler->getById($travelId);

        if (null == $travel || !$travel->isTimeLineActive()) {
            throw new NotFoundHttpException('Travel Not Found');
        }

        $user = $this->getUser();

        $isOwner = false;

        if (null != $user) {
            $isOwner = $travelHandler->isOwner($user, $travel);
        }

        if (!$isOwner && !$travel->isPublic()) {
            throw new AccessDeniedException('Travel Is Private');
        }

        return $this->render('TravelBundle:Travel:show-likes.html.twig', array(
            'travel' => $travel,
            'user' => $user,
            'isOwner' => $isOwner
        ));
    }

    /**
     * @Route("/travel/{slug}/{travelId}/social", name="travel_social", requirements={"travelId" = "\d+"})
     *
     * @param $travelId
     */
    public function socialAction($travelId)
    {
        $travelHandler = $this->container->get('travel.handler.travel');

        $travel = $travelHandler->getById($travelId);

        if (null == $travel || !$travel->isTimeLineActive()) {
            throw new NotFoundHttpException('Travel Not Found');
        }

        $user = $this->getUser();

        $isOwner = false;

        if (null != $user) {
            $isOwner = $travelHandler->isOwner($user, $travel);
        }

        if (!$isOwner && !$travel->isPublic()) {
            throw new AccessDeniedException('Travel Is Private');
        }

        return $this->render('TravelBundle:Travel:social.html.twig', array(
            'travel' => $travel,
            'user' => $user,
            'isOwner' => $isOwner
        ));
    }
}

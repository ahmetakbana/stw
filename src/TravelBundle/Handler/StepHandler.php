<?php

namespace TravelBundle\Handler;

use AppBundle\Handler\MediaHandler;
use Application\Sonata\UserBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use PointBundle\Entity\Point;
use TimeLineBundle\Handler\TimeLineHandler;
use TravelBundle\Entity\Step;
use TravelBundle\Entity\Travel;

class StepHandler
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var TimeLineHandler
     */
    private $timeLineHandler;

    /**
     * @var MediaHandler
     */
    private $mediaHandler;

    /**
     * @param EntityManager $entityManager
     * @param TimeLineHandler $timeLineHandler
     */
    public function __construct(EntityManager $entityManager, TimeLineHandler $timeLineHandler, MediaHandler $mediaHandler)
    {
        $this->entityManager = $entityManager;
        $this->timeLineHandler = $timeLineHandler;
        $this->mediaHandler = $mediaHandler;
    }

    /**
     * @param User $user
     * @param Travel $travel
     * @param array $location
     * @return Step
     */
    public function create(User $user, Travel $travel, array $location)
    {
        $pointRepository = $this->entityManager->getRepository('PointBundle:Point');

        $existingPoint = $pointRepository->findOneBy(array('placeId' => $location[5]));

        if (null == $existingPoint) {
            $point = new Point();
            $point->setName($location[0]);
            $point->setLatitude($location[1]);
            $point->setLongitude($location[2]);
            $point->setAddress($location[3]);
            if ('' != $location[4]) {
                $media = $this->mediaHandler->createFromUrl($location[4], $this->entityManager, 'point');
            } else {
                $media = $this->mediaHandler->createDefaultPointImage($this->entityManager);
            }
            $point->setImage($media);
            $point->setPlaceId($location[5]);
            $point->setCreator($user);
            $this->entityManager->persist($point);
        } else {
            $point = $existingPoint;
        }

        $point->setPopularity($point->getPopularity()+1);

        $step = new Step();
        $step->setName($location[0]);
        $step->setLatitude($location[1]);
        $step->setLongitude($location[2]);
        $step->setAddress($location[3]);
        $step->setPoint($point);
        $step->setCreator($user);
        $travel->addStep($step);

        $user->addTravel($travel);

        $this->entityManager->persist($user);
        $this->entityManager->flush();

        $this->timeLineHandler->userAddStep($user, $travel, $step);

        return $step;
    }

    /**
     * @param int $stepId
     * @return null|Step
     */
    public function getById($stepId)
    {
        $stepRepository = $this->entityManager->getRepository('TravelBundle:Step');

        return $stepRepository->findOneBy(array('id' => $stepId));
    }

    /**
     * @param Step $step
     */
    public function save(Step $step)
    {
        $this->entityManager->persist($step);
        $this->entityManager->flush();
    }

    /**
     * @param Step $step
     */
    public function remove(Step $step)
    {
        $this->timeLineHandler->userRemoveStep($step);

        $this->entityManager->remove($step);
        $this->entityManager->flush();
    }
}
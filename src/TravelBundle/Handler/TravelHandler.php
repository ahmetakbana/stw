<?php

namespace TravelBundle\Handler;

use AppBundle\Handler\MediaHandler;
use Application\Sonata\UserBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use PlaceBundle\Entity\Type;
use PointBundle\Entity\Point;
use TimeLineBundle\Handler\TimeLineHandler;
use TravelBundle\Entity\Step;
use TravelBundle\Entity\Travel;
use TravelBundle\Entity\TravelLike;

class TravelHandler
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var TimeLineHandler
     */
    private $timeLineHandler;

    /**
     * @var MediaHandler
     */
    private $mediaHandler;

    /**
     * @param EntityManager $entityManager
     * @param TimeLineHandler $timeLineHandler
     */
    public function __construct(EntityManager $entityManager, TimeLineHandler $timeLineHandler, MediaHandler $mediaHandler)
    {
        $this->entityManager = $entityManager;
        $this->timeLineHandler = $timeLineHandler;
        $this->mediaHandler = $mediaHandler;
    }

    /**
     * Create travel
     *
     * @param User $user
     * @param array $locations
     * @return Travel
     */
    public function create(User $user, Travel $travel, Array $locations)
    {
        $pointRepository = $this->entityManager->getRepository('PointBundle:Point');

        $types = $this->findTypes();

        $order = 1;
        foreach ($locations as $location) {

            $existingPoint = $pointRepository->findOneBy(array('placeId' => $location[5]));

            if (null == $existingPoint) {
                $point = new Point();
                $point->setName($location[0]);
                $point->setLatitude($location[1]);
                $point->setLongitude($location[2]);
                $point->setAddress($location[3]);
                if ('' != $location[4]) {
                    $media = $this->mediaHandler->createFromUrl($location[4], $this->entityManager, 'point');
                } else {
                    $media = $this->mediaHandler->createDefaultPointImage($this->entityManager);
                }
                $point->setImage($media);
                $point->setPlaceId($location[5]);
                $point->setCreator($user);

                foreach ($types as $type) {
                    if (in_array($type->getType(), $location[6])) {
                        $point->addType($type);
                    }
                }

                if (0 == $point->getTypes()->count()) {
                    if (!empty($location[6])) {
                        $point->setTypeString(@serialize($location[6]));
                    }
                }

                $this->entityManager->persist($point);
            } else {
                $point = $existingPoint;
            }

            $point->setPopularity($point->getPopularity()+1);

            $step = new Step();
            $step->setName($location[0]);
            $step->setLatitude($location[1]);
            $step->setLongitude($location[2]);
            $step->setAddress($location[3]);
            $step->setMode($location[7]);
            $step->setDetailActive((bool) $location[8]);
            $step->setPoint($point);
            $step->setStepOrder($order);
            $step->setCreator($user);
            $travel->addStep($step);

            $order++;
        }

        $travelLike = new TravelLike();
        $travelLike->setTravel($travel);

        $user->addTravel($travel);

        $this->entityManager->persist($travelLike);
        $this->entityManager->persist($user);
        $this->entityManager->flush();

        $this->timeLineHandler->userCreateTravel($user, $travel);

        return $travel;
    }

    /**
     * @param Travel $travel
     * @return Travel
     */
    public function save(Travel $travel)
    {
        $this->entityManager->persist($travel);
        $this->entityManager->flush();

        return $travel;
    }

    /**
     * @param Travel $travel
     */
    public function remove(Travel $travel)
    {
        $this->timeLineHandler->userRemoveTravel($travel);

        $this->entityManager->remove($travel);
        $this->entityManager->flush();
    }

    /**
     * @param $travelId
     * @return Travel
     */
    public function getById($travelId)
    {
        $travelRepository = $this->entityManager->getRepository('TravelBundle:Travel');

        return $travelRepository->findOneBy(array('id' => $travelId));
    }

    /**
     * @param Travel $travel
     * @return array|\Traversable
     */
    public function getTimeLine(Travel $travel, $limit = 10)
    {
        return $this->timeLineHandler->getTravelTimeLine($travel, $limit);
    }

    /**
     * @param User $user
     * @param Travel $travel
     * @return bool
     */
    public function isOwner(User $user, Travel $travel)
    {
        if ($travel->getUser()->getId() == $user->getId() || in_array('ROLE_SUPER_ADMIN', $user->getRoles())) {
            return true;
        }

        return false;
    }

    /**
     * @return Type[]
     */
    public function findTypes()
    {
        $placeTypeRepository = $this->entityManager->getRepository('PlaceBundle:Type');

        return $placeTypeRepository->findAll();
    }

    /**
     * @param User $user
     * @param Travel $travel
     */
    public function like(User $user, Travel $travel)
    {
        $travel->getTravelLike()->addUser($user);

        $this->entityManager->persist($travel);
        $this->entityManager->flush();
    }

    /**
     * @param User $user
     * @param Travel $travel
     */
    public function dislike(User $user, Travel $travel)
    {
        $travel->getTravelLike()->removeUser($user);

        $this->entityManager->persist($travel);
        $this->entityManager->flush();
    }
}
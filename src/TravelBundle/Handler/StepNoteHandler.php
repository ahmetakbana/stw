<?php

namespace TravelBundle\Handler;

use Application\Sonata\UserBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use TimeLineBundle\Handler\TimeLineHandler;
use TravelBundle\Entity\Step;
use TravelBundle\Entity\StepNote;
use TravelBundle\Entity\Travel;

class StepNoteHandler
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var TimeLineHandler
     */
    private $timeLineHandler;

    /**
     * @param EntityManager $entityManager
     * @param TimeLineHandler $timeLineHandler
     */
    public function __construct(EntityManager $entityManager, TimeLineHandler $timeLineHandler)
    {
        $this->entityManager = $entityManager;
        $this->timeLineHandler = $timeLineHandler;
    }

    /**
     * @param User $user
     * @param Step $step
     * @param StepNote $stepNote
     * @return Travel
     */
    public function save(User $user, Step $step, StepNote $stepNote)
    {
        $stepNote->setUser($user);
        $step->addStepNote($stepNote);
        $this->entityManager->persist($step);
        $this->entityManager->flush();

        $travel = $step->getTravel();

        $this->timeLineHandler->userAddStepNote($user, $travel, $stepNote);

        return $travel;
    }

    /**
     * @param StepNote $stepNote
     */
    public function remove(StepNote $stepNote)
    {
        $this->timeLineHandler->userRemoveStepNote($stepNote);

        $this->entityManager->remove($stepNote);
        $this->entityManager->flush();
    }

    /**
     * @param $stepNoteId
     * @return StepNote
     */
    public function getById($stepNoteId)
    {
        $stepNoteRepository = $this->entityManager->getRepository('TravelBundle:StepNote');

        return $stepNoteRepository->findOneBy(array('id' => $stepNoteId));
    }
}
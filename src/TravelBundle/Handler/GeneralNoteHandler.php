<?php

namespace TravelBundle\Handler;

use Application\Sonata\UserBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use TimeLineBundle\Handler\TimeLineHandler;
use TravelBundle\Entity\GeneralNote;
use TravelBundle\Entity\Travel;

class GeneralNoteHandler
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var TimeLineHandler
     */
    private $timeLineHandler;

    /**
     * @param EntityManager $entityManager
     * @param TimeLineHandler $timeLineHandler
     */
    public function __construct(EntityManager $entityManager, TimeLineHandler $timeLineHandler)
    {
        $this->entityManager = $entityManager;
        $this->timeLineHandler = $timeLineHandler;
    }

    /**
     * @param User $user
     * @param Travel $travel
     * @param GeneralNote $generalNote
     * @return Travel
     */
    public function save(User $user, Travel $travel, GeneralNote $generalNote)
    {
        $generalNote->setUser($user);
        $travel->addGeneralNote($generalNote);
        $this->entityManager->persist($generalNote);
        $this->entityManager->persist($travel);
        $this->entityManager->flush();

        $this->timeLineHandler->userAddGeneralNote($user, $travel, $generalNote);

        return $travel;
    }

    /**
     * @param GeneralNote $generalNote
     */
    public function remove(GeneralNote $generalNote)
    {
        $this->timeLineHandler->userRemoveGeneralNote($generalNote);

        $this->entityManager->remove($generalNote);
        $this->entityManager->flush();
    }

    /**
     * @param $generalNoteId
     * @return GeneralNote
     */
    public function getById($generalNoteId)
    {
        $generalNoteRepository = $this->entityManager->getRepository('TravelBundle:GeneralNote');

        return $generalNoteRepository->findOneBy(array('id' => $generalNoteId));
    }
}
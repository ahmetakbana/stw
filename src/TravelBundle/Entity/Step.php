<?php

namespace TravelBundle\Entity;

use AppBundle\Traits\CreatorTrait;
use AppBundle\Traits\SlugTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use PlaceBundle\Entity\PlaceItem;
use PointBundle\Entity\Point;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * Step
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="TravelBundle\Entity\StepRepository")
 */
class Step
{
    use CreatorTrait;
    use SlugTrait;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var int
     *
     * @ORM\Column(name="step_order", type="integer")
     */
    private $stepOrder = 0;

    /**
     * @var float
     *
     * @ORM\Column(name="latitude", type="float")
     */
    private $latitude;

    /**
     * @var float
     *
     * @ORM\Column(name="longitude", type="float")
     */
    private $longitude;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string", length=255, nullable=true)
     */
    private $address;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="arrive_date", type="datetime", nullable=true)
     */
    private $arriveDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="leave_date", type="datetime", nullable=true)
     */
    private $leaveDate;

    /**
     * Travel routing type
     *
     * @var string
     * @ORM\Column(name="mode", type="string")
     */
    private $mode;

    /**
     * @var boolean
     *
     * @ORM\Column(name="detail_active", type="boolean")
     */
    private $detailActive = true;

    /**
     * @var Travel
     *
     * @ORM\ManyToOne(targetEntity="TravelBundle\Entity\Travel", inversedBy="steps", cascade={"persist"})
     */
    private $travel;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="TravelBundle\Entity\StepNote", mappedBy="step", cascade={"persist", "remove"})
     */
    private $stepNotes;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="PlaceBundle\Entity\PlaceItem", mappedBy="step", cascade={"persist", "remove"})
     * @ORM\OrderBy({"itemOrder" = "ASC"})
     */
    private $placeItems;

    /**
     * @var Collection
     *
     * @ORM\ManyToOne(targetEntity="PointBundle\Entity\Point")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $point;

    public function __construct()
    {
        $this->stepNotes = new ArrayCollection();
        $this->placeItems = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Step
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return int
     */
    public function getStepOrder()
    {
        return $this->stepOrder;
    }

    /**
     * @param int $stepOrder
     * @return Step
     */
    public function setStepOrder($stepOrder)
    {
        $this->stepOrder = $stepOrder;

        return $this;
    }

    /**
     * Set latitude
     *
     * @param float $latitude
     * @return Step
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;

        return $this;
    }

    /**
     * Get latitude
     *
     * @return float 
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * Set longitude
     *
     * @param float $longitude
     * @return Step
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;

        return $this;
    }

    /**
     * Get longitude
     *
     * @return float 
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * Set address
     *
     * @param string $address
     * @return Step
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string 
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @return \DateTime
     */
    public function getLeaveDate()
    {
        return $this->leaveDate;
    }

    /**
     * @param \DateTime $leaveDate
     *
     * @return Step
     */
    public function setLeaveDate($leaveDate)
    {
        $this->leaveDate = $leaveDate;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getArriveDate()
    {
        return $this->arriveDate;
    }

    /**
     * @param \DateTime $arriveDate
     *
     * @return Step
     */
    public function setArriveDate($arriveDate)
    {
        $this->arriveDate = $arriveDate;

        return $this;
    }

    /**
     * @return string
     */
    public function getMode()
    {
        return $this->mode;
    }

    /**
     * @param string $mode
     *
     * @return Step
     */
    public function setMode($mode)
    {
        $this->mode = $mode;

        return $this;
    }

    /**
     * @return boolean
     */
    public function isDetailActive()
    {
        return $this->detailActive;
    }

    /**
     * @param boolean $detailActive
     *
     * @return Step
     */
    public function setDetailActive($detailActive)
    {
        $this->detailActive = $detailActive;

        return $this;
    }

    /**
     * Set travel
     *
     * @param Travel $travel
     * @return Step
     */
    public function setTravel(Travel $travel)
    {
        $this->travel = $travel;

        return $this;
    }

    /**
     * Get travel
     *
     * @return Travel
     */
    public function getTravel()
    {
        return $this->travel;
    }

    /**
     * Add step note
     *
     * @param StepNote $generalNote
     * @return Step
     */
    public function addStepNote(StepNote $stepNote)
    {
        $this->stepNotes[] = $stepNote;

        $stepNote->setStep($this);

        return $this;
    }

    /**
     * Remove step note
     *
     * @param GeneralNote $stepNote
     * @return Step
     */
    public function removeStepNote(StepNote $stepNote)
    {
        $this->stepNotes->removeElement($stepNote);

        return $this;
    }

    /**
     * Get step notes
     *
     * @return Collection
     */
    public function getStepNotes()
    {
        return $this->stepNotes;
    }

    /**
     * @param PlaceItem $placeItem
     * @return Step
     */
    public function addPlaceItem(PlaceItem $placeItem)
    {
        $placeItem->setItemOrder($this->getMaxPlaceItemOrder()+1);

        $this->placeItems[] = $placeItem;

        $placeItem->setStep($this);

        return $this;
    }

    /**
     * @param PlaceItem $placeItem
     * @return Step
     */
    public function removePlaceItem(PlaceItem $placeItem)
    {
        $this->placeItems->removeElement($placeItem);

        return $this;
    }

    /**
     * @return Collection
     */
    public function getPlaceItems()
    {
        return $this->placeItems;
    }

    /**
     * @param Point $point
     * @return Step
     */
    public function setPoint(Point $point)
    {
        $this->point = $point;

        return $this;
    }

    /**
     * @return Point
     */
    public function getPoint()
    {
        return $this->point;
    }

    /**
     * @return integer
     */
    public function getMaxPlaceItemOrder()
    {
        if (0 != count($this->getPlaceItems())) {
            return $this->getPlaceItems()->last()->getItemOrder();
        }

        return 0;
    }

    /**
     * @param ExecutionContextInterface $context
     *
     * @Assert\Callback()
     */
    public function validateLeaveDateGreaterThanArriveDate(ExecutionContextInterface $context)
    {
        if (null != $this->arriveDate && null != $this->leaveDate) {
            if ($this->arriveDate->getTimestamp() > $this->leaveDate->getTimestamp()) {
                $context->buildViolation('Leave Date date must be greater than Arrive Date!')
                    ->atPath('leaveDate')
                    ->addViolation();
            }
        }
    }
}

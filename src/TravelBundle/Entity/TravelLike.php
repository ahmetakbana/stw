<?php

namespace TravelBundle\Entity;

use Application\Sonata\UserBundle\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * TravelLike
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="TravelBundle\Entity\TravelLikeRepository")
 */
class TravelLike
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Travel
     *
     * @ORM\OneToOne(targetEntity="TravelBundle\Entity\Travel", inversedBy="travelLike", cascade={"persist"})
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $travel;

    /**
     * @var User[]
     *
     * @ORM\ManyToMany(targetEntity="Application\Sonata\UserBundle\Entity\User")
     */
    private $users;


    public function __construct()
    {
        $this->users = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param Travel $travel
     * @return TravelLike
     */
    public function setTravel(Travel $travel)
    {
        $this->travel = $travel;

        return $this;
    }

    /**
     * @return Travel
     */
    public function getTravel()
    {
        return $this->travel;
    }

    /**
     * @param User $user
     * @return TravelLike
     */
    public function addUser(User $user)
    {
        $this->users[] = $user;

        return $this;
    }

    /**
     * @param User $user
     * @return TravelLike
     */
    public function removeUser(User $user)
    {
        $this->users->removeElement($user);

        return $this;
    }

    /**
     * @return User[]
     */
    public function getUsers()
    {
        return $this->users;
    }
}

<?php

namespace TravelBundle\Entity;

use AppBundle\Traits\CreatedUpdatedTrait;
use Application\Sonata\UserBundle\Entity\User;
use Doctrine\ORM\Mapping as ORM;

/**
 * StepNote
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="TravelBundle\Entity\StepNoteRepository")
 */
class StepNote
{
    use CreatedUpdatedTrait;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="text", type="text")
     */
    private $text;

    /**
     * @var Travel
     *
     * @ORM\ManyToOne(targetEntity="TravelBundle\Entity\Step", inversedBy="stepNotes", cascade={"persist"})
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $step;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="Application\Sonata\UserBundle\Entity\User")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $user;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set text
     *
     * @param string $text
     * @return StepNote
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Get text
     *
     * @return string 
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Set step
     *
     * @param Step $step
     * @return StepNote
     */
    public function setStep(Step $step)
    {
        $this->step = $step;

        return $this;
    }

    /**
     * Get step
     *
     * @return Step
     */
    public function getStep()
    {
        return $this->step;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     * @return GeneralNote
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }
}

<?php

namespace TravelBundle\Entity;

use AppBundle\Traits\CreatedUpdatedTrait;
use AppBundle\Traits\SlugTrait;
use Application\Sonata\UserBundle\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Travel
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="TravelBundle\Entity\TravelRepository")
 */
class Travel
{
    use SlugTrait;
    use CreatedUpdatedTrait;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     *
     * @Assert\NotBlank(message="Please type travel name")
     * @Assert\Length(max=100)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", length=1500, nullable=true)
     *
     * @Assert\Length(max=1500)
     */
    private $description;

    /**
     * @var boolean
     *
     * @ORM\Column(name="public", type="boolean")
     */
    private $public;

    /**
     * @var boolean
     *
     * @ORM\Column(name="time_line_active", type="boolean")
     */
    private $timeLineActive;

    /**
     * @var boolean
     *
     * @ORM\Column(name="time_table_active", type="boolean")
     */
    private $timeTableActive;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="TravelBundle\Entity\Step", mappedBy="travel", cascade={"persist", "remove"})
     * @ORM\OrderBy({"stepOrder" = "ASC"})
     */
    private $steps;

    /**
     * @var User
     *
     * @ORM\ManyToOne(
     *     targetEntity="Application\Sonata\UserBundle\Entity\User",
     *     inversedBy="travels",
     *     cascade={"persist"}
     * )
     */
    private $user;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="TravelBundle\Entity\GeneralNote", mappedBy="travel", cascade={"persist", "remove"})
     */
    private $generalNotes;

    /**
     * @var TravelLike
     *
     * @ORM\OneToOne(targetEntity="TravelBundle\Entity\TravelLike", mappedBy="travel", cascade={"persist", "remove"})
     */
    private $travelLike;


    public function __construct()
    {
        $this->steps = new ArrayCollection();
        $this->generalNotes = new ArrayCollection();
        $this->public = true;
        $this->timeLineActive = true;
        $this->timeTableActive = true;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Travel
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Travel
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return boolean
     */
    public function isPublic()
    {
        return $this->public;
    }

    /**
     * @param boolean $public
     * @return Travel
     */
    public function setPublic($public)
    {
        $this->public = $public;

        return $this;
    }

    /**
     * @return boolean
     */
    public function isTimeLineActive()
    {
        return $this->timeLineActive;
    }

    /**
     * @param boolean $timeLineActive
     * @return Travel
     */
    public function setTimeLineActive($timeLineActive)
    {
        $this->timeLineActive = $timeLineActive;

        return $this;
    }

    /**
     * @return boolean
     */
    public function isTimeTableActive()
    {
        return $this->timeTableActive;
    }

    /**
     * @param boolean $timeTableActive
     * @return Travel
     */
    public function setTimeTableActive($timeTableActive)
    {
        $this->timeTableActive = $timeTableActive;

        return $this;
    }

    /**
     * Add step
     *
     * @param Step $step
     * @return Travel
     */
    public function addStep(Step $step)
    {
        $step->setStepOrder($this->getMaxStepOrder()+1);

        $this->steps[] = $step;

        $step->setTravel($this);

        return $this;
    }

    /**
     * Remove step
     *
     * @param Step $step
     * @return Travel
     */
    public function removeStep(Step $step)
    {
        $this->steps->removeElement($step);

        return $this;
    }

    /**
     * Get steps
     *
     * @return Collection
     */
    public function getSteps()
    {
        return $this->steps;
    }

    /**
     * Set user
     *
     * @param User $user
     * @return Travel
     */
    public function setUser(User $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get User
     *
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Add general note
     *
     * @param GeneralNote $generalNote
     * @return Travel
     */
    public function addGeneralNote(GeneralNote $generalNote)
    {
        $this->generalNotes[] = $generalNote;

        $generalNote->setTravel($this);

        return $this;
    }

    /**
     * Remove general note
     *
     * @param GeneralNote $generalNote
     * @return Travel
     */
    public function removeGeneralNote(GeneralNote $generalNote)
    {
        $this->generalNotes->removeElement($generalNote);

        return $this;
    }

    /**
     * Get general notes
     *
     * @return Collection
     */
    public function getGeneralNotes()
    {
        return $this->generalNotes;
    }

    /**
     * @param TravelLike $travelLike
     * @return Travel
     */
    public function setTravelLike(TravelLike $travelLike)
    {
        $this->travelLike = $travelLike;

        return $this;
    }

    /**
     * @return TravelLike
     */
    public function getTravelLike()
    {
        return $this->travelLike;
    }

    /**
     * @return integer
     */
    public function getMaxStepOrder()
    {
        if (0 != count($this->getSteps())) {
            return $this->getSteps()->last()->getStepOrder();
        }

        return 0;
    }
}

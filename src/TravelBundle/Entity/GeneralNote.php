<?php

namespace TravelBundle\Entity;

use AppBundle\Traits\CreatedUpdatedTrait;
use Application\Sonata\UserBundle\Entity\User;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * GeneralNote
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="TravelBundle\Entity\GeneralNoteRepository")
 */
class GeneralNote
{
    use CreatedUpdatedTrait;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="text", type="text", length=1000)
     *
     * @Assert\NotBlank(message="Please type note")
     * @Assert\Length(max=1000)
     */
    private $text;

    /**
     * @var Travel
     *
     * @ORM\ManyToOne(targetEntity="TravelBundle\Entity\Travel", inversedBy="generalNotes", cascade={"persist"})
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $travel;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="Application\Sonata\UserBundle\Entity\User")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $user;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set text
     *
     * @param string $text
     * @return GeneralNote
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Get text
     *
     * @return string 
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Set travel
     *
     * @param Travel $travel
     * @return GeneralNote
     */
    public function setTravel(Travel $travel)
    {
        $this->travel = $travel;

        return $this;
    }

    /**
     * Get travel
     *
     * @return Travel
     */
    public function getTravel()
    {
        return $this->travel;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     * @return GeneralNote
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }
}

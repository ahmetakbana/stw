<?php

namespace TravelBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class StepNoteType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, Array $options)
    {
        $builder
            ->add('text', 'textarea')
        ;
    }

    public function getName()
    {
        return 'travel_step_note';
    }
}
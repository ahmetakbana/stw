<?php

namespace TravelBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class StepTimeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, Array $options)
    {
        $builder
            ->add('arriveDate', 'datetime', array(
                'widget' => 'single_text'
            ))
            ->add('leaveDate', 'datetime', array(
                'widget' => 'single_text'
            ))
        ;
    }

    public function getName()
    {
        return 'travel_step_time';
    }
}
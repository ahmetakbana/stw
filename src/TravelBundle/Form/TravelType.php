<?php

namespace TravelBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class TravelType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, Array $options)
    {
        $builder
            ->add('name')
            ->add('description', 'textarea')
        ;
    }

    public function getName()
    {
        return 'travel_travel';
    }
}
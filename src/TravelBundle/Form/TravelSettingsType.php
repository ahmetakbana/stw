<?php

namespace TravelBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\FormBuilderInterface;

class TravelSettingsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, Array $options)
    {
        $builder
            ->add('public', CheckboxType::class, array(
                'label'    => 'Show this travel publicly?',
                'required' => false,
            ))
            ->add('timeLineActive', CheckboxType::class, array(
                'label'    => 'Show timeline?',
                'required' => false,
            ))
            ->add('timeTableActive', CheckboxType::class, array(
                'label'    => 'Show time table?',
                'required' => false,
            ))
        ;
    }

    public function getName()
    {
        return 'travel_settings';
    }
}
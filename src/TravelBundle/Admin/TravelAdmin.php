<?php

namespace TravelBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ProxyQueryInterface;
use Sonata\AdminBundle\Form\FormMapper;
use TravelBundle\Entity\Travel;

class TravelAdmin extends Admin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('name', 'text')
            ->add('description', 'textarea')
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('name')
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('name')
            ->add('user')
            ->add('_action', null, array(
                'actions' => array(
                    'delete' => array(),
                    'preview' => array(
                        'template' => 'TravelBundle:Admin:list__action_preview_travel.html.twig'
                    )
                )
            ))
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function preRemove($travel)
    {
        $this->removeTimeLine($travel);
        parent::preRemove($travel);
    }

    /**
     * {@inheritdoc}
     */
    public function preBatchAction($actionName, ProxyQueryInterface $query, array &$idx, $allElements)
    {
        if ('delete' == $actionName) {
            $container = $this->getConfigurationPool()->getContainer();
            $travelRepository = $container->get('travel.repository.travel');

            foreach ($idx as $travelId) {
                $travel = $travelRepository->findOneBy(array('id' => $travelId));
                $this->removeTimeLine($travel);
            }
        }
    }

    /**
     * @param Travel $place
     */
    protected function removeTimeLine(Travel $travel)
    {
        $container = $this->getConfigurationPool()->getContainer();

        $timeLineHandler = $container->get('time_line.handler');

        $timeLineHandler->userRemoveTravel($travel);
    }
}
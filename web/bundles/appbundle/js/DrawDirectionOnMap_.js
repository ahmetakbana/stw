/**
 * The initial Map center.
 */
var mapCenter = [11.313430592259937, 21.402397366699233];

/**
 * The initial Map zoom level.
 */
var mapZoom = 2;

/**
 * Data for the markers consisting of a name, a latitude, longitude and address
 * @type {Array}
 */
var locations = [];

/**
 * color of the marker on the map
 */
var markerColor = 'default';

/**
 * Line Color
 */
var polyLineColor = '#E82087';

/**
 * Google Map
 */
var googleMap;

/**
 * Auto complete place
 */
var place;

/**
 * Marker
 * @type {Array}
 */
var marker = [];

/**
 * Map Line
 */
var polyLine;

function initialize() {

    var config = {
        zoom: mapZoom,
        mapTypeControl: false,
        streetViewControl: false,
        disableDoubleClickZoom: true,
        center: new google.maps.LatLng(mapCenter[0], mapCenter[1])
    };
    googleMap = new google.maps.Map(document.getElementById("map_canvas"), config);

    var input = /** @type {HTMLInputElement} */(document.getElementById('pac-input'));

    var types = document.getElementById('type-selector');
    googleMap.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
    googleMap.controls[google.maps.ControlPosition.TOP_LEFT].push(types);

    var autoComplete = new google.maps.places.Autocomplete(input);

    google.maps.event.addListener(autoComplete, 'place_changed', function () {

        var place = autoComplete.getPlace();

        if (!place.geometry) {
            return;
        }

        var loc = place.geometry.location;

        var latitude = loc.lat(function ($a) {
            return $a;
        });

        var longitude = loc.lng(function ($a, $b) {
            return $b;
        });

        if (locations.length > 0) {
            deleteMap();
        }

        locations.push([place.name, latitude, longitude, place.adr_address]);

        makeMap();

        createChart();

    });

}

function deleteMap() {

    for (var i = 0; i < locations.length; i++) {
        marker[i].setMap(null);
    }
    polyLine.setMap(null);
}

function makeMap() {

    var lt = new google.maps.LatLngBounds();
    for (var d = 0; d < locations.length; d++) {
        var c = locations[d];
        var g = new google.maps.LatLng(c[1], c[2]);
        marker[d] = new google.maps.Marker({
            position: g,
            shadow: shadowImage(),
            icon: markerImage(d, locations.length),
            shape: markerShape(),
            title: c[0],
            zIndex: d,
            map: googleMap
        });
        marker[d].setMap(googleMap);
    }

    setPolyLine();

    if (locations.length > 1) {
        for (var l = 0; l < locations.length; l++) {
            var cl = locations[l];
            var gl = new google.maps.LatLng(cl[1], cl[2]);
            lt.extend(gl);
        }
        googleMap.fitBounds(lt);
    } else {
        ltc = locations[0];
        googleMap.setCenter(new google.maps.LatLng(ltc[1], ltc[2]));
        googleMap.setZoom(9);
    }

}

function setPolyLine() {
    var path = [];
    for (var i = 0; i < locations.length; i++) {
        var location = locations[i];
        path.push(new google.maps.LatLng(location[1], location[2]))
    }
    polyLine = new google.maps.Polyline({
        path: path,
        strokeColor: polyLineColor,
        strokeOpacity: 1,
        strokeWeight: 2,
        visible: true
    });
    polyLine.setMap(googleMap)
}


function markerImage(b, a) {
    var c = "stop";
    if (0 == b) {
        c = "start"
    } else {
        if (b == (a - 1)) {
            c = "end"
        }
    }

    return new google.maps.MarkerImage(
        "../bundles/appbundle/images/" + c + "_marker_" + markerColor + ".png",
        new google.maps.Size(19, 37),
        new google.maps.Point(0, 0),
        new google.maps.Point(9, 37)
    );

}


function shadowImage() {

    return new google.maps.MarkerImage(
        "../bundles/appbundle/images/marker_shadow.png",
        new google.maps.Size(37, 34),
        new google.maps.Point(0, 0),
        new google.maps.Point(9, 34)
    );
}


function markerShape() {
    var a = {
        coord: [1, 1, 1, 20, 18, 20, 18, 1],
        type: "poly"
    }
}

function createChart() {

    var travelChart = $('.trv-chart');

    travelChart.empty();

    for (key in locations) {

        $('.trv-chart-button-main').remove();
        travelChart.append(addStepHtml(key));
        travelChart.append(addSaveButtonHtml());

        //Address Format
        var adr;
        var country = $('#trv-chart-' + key + ' .trv-chart-body .text-success .country-name').html();
        adr = country;

        var region = $('#trv-chart-' + key + ' .trv-chart-body .text-success .region').html();
        if (region) {
            adr = region + ' / ' + adr;
        }

        var locality = $('#trv-chart-' + key + ' .trv-chart-body .text-success .locality').html();
        if (locality) {
            adr = locality + ' - ' + adr;
        }

        var street = $('#trv-chart-' + key + ' .trv-chart-body .text-success .street-address').html();
        if (street) {
            adr = street + ' - ' + adr;
        }

        if (!street && !locality) {
            adr = country;
        }

        var addressText = $('#trv-chart-' + key + ' .trv-chart-body .text-success');
        addressText.empty();
        addressText.html(adr);
        //Address Format End

    }

    var locInput = $('#pac-input');
    locInput.val('');

    if (locations.length != 0) {
        locInput.attr('placeholder', 'Enter '+ locations.length +'. step');
    }

    locInput.focus();

}

function deleteChart(locationIndex) {
    if (locations.length == 1) {
        locations.splice(locationIndex, 1);
        $('#trv-map-main').empty();
        $('#trv-map-main').append(addMapHtml());
        initialize();
    } else {
        locations.splice(locationIndex, 1);
        deleteMap();
        makeMap();
    }
    createChart();
}

/**
 * Adds Map html
 * @returns {string}
 */
function addMapHtml()
{
    return  '<input id="pac-input" class="controls" type="text" placeholder="Enter start point">' +
            '<div id="map_canvas" style="width: 100%; height: 400px"></div>'
    ;
}

/**
 * Adds Save button
 * @returns {string}
 */
function addSaveButtonHtml()
{
    return  '<div class="trv-chart-button-main">' +
                '<div class="trv-chart-button">' +
                    '<div>' +
                        '<a href="" class="btn-u" style="background-color: #5bc0de;">Save</a>' +
                    '</div>' +
                '</div>' +
            '</div>'
    ;
}

/**
 * Adds Step frame to chart
 * @param key
 * @returns {string}
 */
function addStepHtml(key)
{
    placeName = locations[key][0];
    address = locations[key][3];

    step = parseInt(key);
    if (key == 0) {
        var stepTitle = 'Start';
        $('.trv-chart').empty();
    } else {
        var stepTitle = (step) + ' Step';
    }

    return  '<div id="trv-chart-' + (step) + '">' +
                '<div class="trv-chart-main">' +
                    '<div>' +
                        '<span class="label label-warning"><i class="icon-map-marker"></i></span>' +
                        '<strong><span class="text-warning"> ' + stepTitle + ' :</span> ' + placeName + '</strong>' +
                        '<span id="' + (step) + '" class="pull-right">' +
                        '<button type="button" class="close" onclick="deleteChart(' + key + ')" data-dismiss="alert">×</button>' +
                        '</span>' +
                    '</div>' +
                    '<div class="trv-chart-line"></div>' +
                    '<div class="trv-chart-body">' +
                        '<span class="text-success">' + address + '</span>' +
                    '</div>' +
                '</div>' +
            '</div>'
    ;
}




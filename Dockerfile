FROM php:7-apache
MAINTAINER Ahmet Akbana <ahmetakbana@gmail.com>

WORKDIR /var/www/html

ENV SYMFONY_ENV prod
ENV SYMFONY_DEBUG 0

# Packages
RUN apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install -y \
  libfreetype6-dev \
  libjpeg62-turbo-dev \
  libmcrypt-dev \
  libpng12-dev \
  libbz2-dev \
  php-pear \
  curl \
  git \
  unzip \
  && docker-php-ext-install mcrypt zip bz2 bcmath pdo_mysql mysqli mbstring opcache \
  && docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/ \
  && docker-php-ext-install gd

RUN ( \
    echo "date.timezone = UTC"; \
    echo "short_open_tag = Off"; \
    echo "magic_quotes_gpc = Off"; \
    echo "register_globals = Off"; \
    echo "session.auto_start = Off"; \
    ) > /usr/local/etc/php/conf.d/symfony-recommended.ini

RUN ( \
    echo "opcache.memory_consumption=128"; \
    echo "opcache.interned_strings_buffer=8"; \
    echo "opcache.max_accelerated_files=4000"; \
    echo "opcache.revalidate_freq=60"; \
    echo "opcache.fast_shutdown=1"; \
    echo "opcache.enable_cli=1"; \
    ) > /usr/local/etc/php/conf.d/opcache-recommended.ini

RUN curl -sS https://getcomposer.org/installer | php ;\
        mv composer.phar /usr/local/bin/composer

COPY ./ /var/www/html/

RUN /usr/local/bin/composer install --no-dev --prefer-dist ;\
    /usr/local/bin/php /var/www/html/app/console doctrine:migration:migrate ;\
    /usr/local/bin/php /var/www/html/app/console assets:install ;\
    mkdir /var/www/html/web/temp ;\
    chown -R www-data:www-data /var/www/html/web/temp ;\
    chown -R www-data:www-data /var/www/html/web/defaultimages ;\
    chmod -R 777 /var/www/html/web/temp ;\
    chmod -R 777 /var/www/html/web/defaultimages/* ;\
    chown -R www-data:www-data /var/www/html/app/cache ;\
    chown -R www-data:www-data /var/www/html/app/logs

RUN a2enmod rewrite
ADD apache-vhost.conf /etc/apache2/sites-available/000-default.conf

EXPOSE 80
CMD ["apache2-foreground"]
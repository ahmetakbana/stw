# vim: set tabstop=8 softtabstop=8 noexpandtab:

install:
	@composer install --no-interaction --prefer-source
	$(MAKE) init

init-clear:
	$(MAKE) clear
	$(MAKE) init

init:
	@rm -Rf web/uploads/media/*
	@rm -Rf app/logs/*.log
	$(MAKE) setup-db
	$(MAKE) fixtures

setup-db:
	@php app/console doctrine:database:drop --force --if-exists
	@php app/console doctrine:database:create
	$(MAKE) migration


fixtures:
	@php app/console doctrine:fixtures:load --append --multiple-transactions

clear:
	@php app/console cache:clear --no-debug
	@php app/console doctrine:cache:clear-metadata --quiet --no-debug
	@php app/console doctrine:cache:clear-query --quiet --no-debug
	@php app/console doctrine:cache:clear-result --quiet --no-debug

migration:
	@php app/console doctrine:migrations:migrate --no-interaction

clean:
	@rm -rf web/uploads/*
	@php app/console cache:clear
	@php app/console assets:install --symlink